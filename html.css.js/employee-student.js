function viewRightSide() {
  var addLeftSide = document.getElementById("right");
  if (addLeftSide.style.display === "block") {
    addLeftSide.style.display = "none";
  } else {
    addLeftSide.style.display = "block";
  }
}

function addForm() {
  var addButton = document.getElementById("form");
  if (addButton.style.display === "block") {
    addButton.style.display = "none";
  } else {
    addButton.style.display = "block";
  }
}

function validateForm() {
  var nameForm = document.getElementById("name").value;
  var areaForm = document.getElementById("area").value;
  var idForm = document.getElementById("id").value;

  if (nameForm === "" || nameForm === null && areaForm === "" || areaForm === null && idForm === "" || idForm === null) {
      alert("Name and Area cannot be Empty or null");
  } else {
    var x = document.getElementsByClassName("grid-items");
    
    x[4].innerHTML = nameForm;
    x[5].innerHTML = areaForm;
    x[6].innerHTML = idForm;
    alert("Form data Successfully added!!");
  }
}
