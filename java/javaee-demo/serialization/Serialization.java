/*
What is serialization? 
 Anwser:
 '''''''
    Serialization is a mechanism of converting the state of an object into a byte stream. 
Deserialization is the reverse process where the byte stream is used to recreate the actual 
Java object in memory. This mechanism is used to persist the object.

What is need of serialization?
 Anwser:
 '''''''
    Serialization is the process of converting an object into a stream of bytes to store the 
object or transmit it to memory, a database, or a file. Its main purpose is to save the state 
of an object in order to be able to recreate it when needed.
 
What will happen if one of the member in the class doesn�t implement serializable interface?
  Anwser:
  '''''''
     When you try to serialize an object which implements Serializable interface, incase if 
the object includes a reference of an non serializable object then NotSerializableException 
will be thrown.  
 */