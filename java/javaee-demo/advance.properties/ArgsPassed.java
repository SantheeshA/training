import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/*
1. Get and print a configurable property based on following priority:
  - args passed for java cmd
  - if not avaialble as cmd args, try to get from System env variables
  - if not available from Sys Env, try to get from a customized .properties file (D:\temp\customize.properties)
  - if not available in customized props files, get from a default .properties file (\\nas\Distribution\training\temp\default.properties)
  - if not available in default props, load from a harcoded default constant
Try to use Properties.setParent() heirarchy mechanism to set out the priorities
*/
public class ArgsPassed {
	public static void main(String args[]) throws IOException {
		
		System.out.println("--------args passed for java cmd --------------");
		//args passed for java cmd 
		for (String string : args)
			System.out.println(string);

		// Get from System env variables
		// of all environment variables at once
		// using System.getenv() method
		System.out.println("--------Get from System env variables--------------");
		Map<String, String> env = System.getenv();

		for (String envName : env.keySet()) {
			System.out.format("%s=%s%n", envName, env.get(envName));
		}
		
		System.out.println("--------Get from a customized .properties file--------------");
		FileInputStream fileInput = new FileInputStream("C:\\Users\\santh\\eclipse-workspace\\JavaEE-Demo\\advance.properties\\customize.properties");
		Properties properties = new Properties();
		
		properties.load(fileInput);
		
		String userName = properties.getProperty("Username");
		String password = properties.getProperty("Password");
		String hostName = properties.getProperty("HostName");
		
		System.out.println(userName);
		System.out.println(password);
		System.out.println(hostName);
		
	}
}
