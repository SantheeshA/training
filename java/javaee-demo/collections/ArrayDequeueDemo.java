/*
 4.Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() 
methods to store and retrieve elements in ArrayDequeue.
---------------------------------------WBS----------------------------------
1.Requirements:
   - Dequeue with elements.
2.Entities
   - ArrayDequeueDemo
3.Function Declaration
   - public static void main(String [] args)
4.Jobs to be done
    1.Create a array dequeue as car.
    2.Add 7 values using addFirst(),addLast(),add methods in the dequeue.
    3.Print peek values using peekFirst(),peekLast() methods in the dequeue.
    4.Remove element using pollFirst,pollLast,removeFirst,removeLast methods in the dequeue.
    5.Print the dequeue.

Psudeo Code:
''''''''''''
public class ArrayDequeueDemo {
	public static void main(String [] args) {
		Deque<String> cars = new LinkedList<String>(); 
		cars.addFirst("Meclaren");  
		Add 5 ELEMENTS
		cars.addLast("Bugati");
		System.out.println("Peek First Value: "+cars.peekFirst()); 
		System.out.println("Peek Last Value: "+cars.peekLast()); 
		cars.pollFirst();  
		cars.pollLast();   
		cars.removeFirst();
		cars.removeLast(); 
		System.out.println(cars);
	}

}
---------------------------------------Program----------------------------------
 */

import java.util.Deque;
import java.util.LinkedList;
public class ArrayDequeueDemo {
	public static void main(String [] args) {
		Deque<String> cars = new LinkedList<String>(); 
		cars.addFirst("Meclaren");  //Add at first
		cars.add("BMW");
		cars.add("Mazda");
		cars.add("Lexus");
		cars.add("Audi");
		cars.addLast("Bugati");   //add at last
		System.out.println("Peek First Value: "+cars.peekFirst()); //Peek first value
		System.out.println("Peek Last Value: "+cars.peekLast()); //Peek last value
		cars.pollFirst();  //poll first value
		cars.pollLast();   //poll last value
		cars.removeFirst();  //remove first value
		cars.removeLast();   //remove last value
		System.out.println(cars);
	}

}
