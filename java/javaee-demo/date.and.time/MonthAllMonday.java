/*
11. Write a Java program for a given month of the current year, lists all of the Mondays in that month.
----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program for a given month of the current year, lists all of the Mondays in that month.

2.Entity:
    - MonthAllMonday

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Invoke Month class valueOf method pass parameters as month with 
toUpperCase method to convert to uppercase
          1.2)Get first monday using with TemporalAdjusters class firstInMonth methof to get first monday.
          1.1)Print the month.
     2.Get the current year, month and date using now, atMonth and atDay method.
          2.1)Store current date in LocalDate class date.
     3.Get the given month using getMonth method store in monthMonday.
     4.Check monthMonday is equal to given month
         4.1)Print date of monday in month
         4.2)Change date next monday in month using with TemporalAdjusters class next method.
         4.3)Get the date using getMonth method and store in date.
     
Pseudo Code:
''''''''''''
public class MonthAllMonday {
    public static void main(String[] args) {
        Month month = Month.valueOf("March".toUpperCase());
        System.out.printf("For the month of %s:%n", month);
        LocalDate date = Year.now().atMonth(month).atDay(1).
              with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        Month monthMonday = date.getMonth();
        while (monthMonday == month) {
            System.out.printf("%s%n", date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            monthMonday = date.getMonth();
        }
    }
}
---------------------------------Program Code------------------------------- */
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;

public class MonthAllMonday {
    public static void main(String[] args) {
        Month month = Month.valueOf("March".toUpperCase());

        System.out.printf("For the month of %s:%n", month);
        LocalDate date = Year.now().atMonth(month).atDay(1).
              with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        Month monthMonday = date.getMonth();
        while (monthMonday == month) {
            System.out.printf("%s%n", date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            monthMonday = date.getMonth();
        }
    }
}