/*
Problem Statement:

1.Given a random date, how would you find the date of the previous Friday
------------------------------WBS---------------------------------------
1.Requirement:
    - To Given a random date, how would you find the date of the previous Friday

2.Entity:
    - PreviousFridayDemo

3.Method Signature:
    - public static void main(String[] args)

4.Jobs to be Done:
    1.Invoke the LocalDate class and get current time using now method.
    2.Using LocalDate class's object get the Previous Friday's date and Print it .
    
Pseudo Code:
''''''''''''

public class PreviousFridayDemo {
    public static void main(String[] args) {     
       LocalDate date = LocalDate.now();
    	System.out.printf("The previous Friday is: %s%n",
    	          date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}
------------------------------Program Code---------------------------------------
*/

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
   

public class RandomDate {
    
    public static void main(String[] args) {
    	LocalDate date = LocalDate.now();
    	System.out.printf("The previous Friday is: %s%n",
    	          date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}