/* Consider a following code snippet
        Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());    

  what will be output and complete the code.
---------------------------------------------------------WBS---------------------------------------------------
1.Requirements :
  - what will be output and complete the code.
2.Entities:
  - QueueSnippet 
3.Function Declaration:
  - public static void main(String [] args)
4.Job to be done :  
   1.Create the Queue with generic String type.
   2.Add 6 elements using add method.
   3.Remove the peek value using poll method.
   4.Print peek value in using peek method.

Psudeo Code:
''''''''''''
public class QueueSnippet{
    public static void main(String [] args) {
    	Queue<String> bikes = new PriorityQueue<String>();
    	ADD 5 ELEMENTS
        bikes.poll();
        System.out.println(bikes.peek());         
    }
}
 ----------------------------------------------------Program Code---------------------------------------------------
*/
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueSnippet{
    public static void main(String [] args) {
    	Queue<String> bikes = new PriorityQueue<String>();//Specifiy type of PriorityQueue
    	bikes.add("Royal Enfield ");
    	bikes.add("FZ");
    	bikes.add("R15");
    	bikes.add("RX100");
        bikes.poll();
        System.out.println(bikes.peek());         
    }
}