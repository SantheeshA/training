/*
7. Consider a following code snippet:
     List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
   - Get the non-duplicate values from the above list using java.util.Stream API

--------------------------------WBS----------------------------------------------

1.Requirement:
    Consider a following code snippet:
      - List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
      - Get the non-duplicate values from the above list using java.util.Stream API

2.Entity:
    - NonDuplicate

3.Function declaration
    - public static void main(String[] args)

4.Jobs to be done:
     1.Convert Array to list using asList method and store it in randomNumbers list.
     2.Filters randomNumbers list using stream method with filter and frequency method.
        2.1)Check randomNumbers list with number equals to one.
        2.2)Collections collect as list using asList method.
        2.3)Print using for each.      

Psudeo Code:
''''''''''''
public class NonDuplicate {
    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        randomNumbers.stream()
        .filter(number -> Collections.frequency(randomNumbers, number) == 1)
        .collect(Collectors.toList())
        .forEach(System.out::println); 
    }
}

--------------------------------Program----------------------------------------------
*/


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NonDuplicate {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        randomNumbers.stream()
        .filter(number -> Collections.frequency(randomNumbers, number) == 1)
        .collect(Collectors.toList())
        .forEach(System.out::println); 
    }

}
