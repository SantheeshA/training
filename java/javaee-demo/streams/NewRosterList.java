/*
12. Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
    - Create the roster from the Person class and add each person in the
        newRoster to the existing list and print the new roster List.
    - Print the number of persons in roster List after the above addition.
    - Remove the all the person in the roster list

------------------------------------------WBS---------------------------------------

1.Requirement:
	- Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
	- Print the number of persons in roster List after the above addition.
	- Remove the all the person in the roster list

2.Entity:
	- NewRosterList

3.Function declaration
	- public static void main(String[] args)

4.Jobs to be done:
     1.Invoke Person class createRoster method and store it in persons List.
     2.For each newRoster 
          2.1)Invoke add method and add new rosters.
          2.2)Print all person name and age using person class printPerson method.
     3.Print the size of roster using size method.
     4.Remove all roasters using clear method.     
     5.Print the size of roster using size method.
              
Psudeo Code:
''''''''''''
public class NewRosterList {
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        newRoster.forEach(roster::add);
        newRoster.forEach(Person::printPerson);
        System.out.println("Number of persons in roster list: " + roster.size());
        roster.clear();
        System.out.println("Number of persons in roster list: " + roster.size());
    }
}
------------------------------------------Program---------------------------------------
*/

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class NewRosterList {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));

        newRoster.forEach(roster::add);
        // adding the objects(elements) in newRoster to roster list
        newRoster.forEach(Person::printPerson);
        // printing the persons in newRoster list
        System.out.println("Number of persons in roster list: " + roster.size());
        // printing the number of persons in list
        roster.clear();
        // removing all the elements of roster list
        System.out.println("Number of persons in roster list: " + roster.size());
    }

}
