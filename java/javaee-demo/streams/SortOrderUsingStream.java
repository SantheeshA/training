/*
11. Sort the roster list based on the person's age in descending order using
java.util.Stream

-------------------------------------WBS-----------------------------------------
1.Requirement:
    - Sort the roster list based on the person's age in descending order using java.util.Stream

2.Entity:
	- SortOrderUsingStream

3.Function declaration
    - public static void main(String[] args)

4.Jobs to be done:
    1.Invoke Person class createRoster method and store it in personList List.
    2.Sort person's age using stream sorted method by compare using Person class compareByAge method.
    3.For each personList print person name and age using Person class printPerson method.
    
Psudeo Code:
'''''''''''
public class SortOrderUsingStream {
    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);
    }
}

-------------------------------------Program-----------------------------------------
*/
import java.util.List;

public class SortOrderUsingStream {

    public static void main(String[] args) {
    	// stream() method can pipeline many different methods to get desired
        // results in single line of code
        List<Person> personList = Person.createRoster();
        personList.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);
        
        // sorted() method accepts comparator here compareByAge method of person
        // class is passed
        // finally forEach to print person details

    }

}
