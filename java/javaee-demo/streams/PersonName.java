/*
2. Write a program to print minimal person with name and email address from the
Person class using java.util.Stream<T>#map API
-------------------------------WBS---------------------------------------
1.Requirement:
  - Write a program to print minimal person with name and email address from the
   Person class using java.util.Stream<T>#map API

2.Entity:
  - PersonNameAndMail

3.Function declaration
  - public static void main(String[] args)

4.Jobs to be done:
   1.Invoke Person class createRoster method and store it in rosterList List.
   2.For each rosterList 
           2.1)Add each person using put method.
   3.Collect person name to map using stream , collect, toMap method
         3.1)Get person name and email using Person class getEmailAddress and getName method.
   4.For each print using entrySet method.
   
Psudeo Code:
'''''''''''
public class PersonName {
    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        Map<String, String> mapDetails = new HashMap<>();
        for (Person roster : rosterList) {
            mapDetails.put(roster.emailAddress, roster.name);
        }
        Map<String, String> rosterMap = rosterList.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        rosterMap.entrySet().forEach(System.out::println);
    }
}
----------------------------Program------------------------------------------------

*/

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonName {

    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();

        Map<String, String> mapDetails = new HashMap<>();

        for (Person roster : rosterList) {
            mapDetails.put(roster.emailAddress, roster.name);
        }

        //Another way to do the same
        Map<String, String> rosterMap = rosterList.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        // Using stream() method the collections of objects parallely processed
        // The collect method act to each and every object.
       
        rosterMap.entrySet().forEach(System.out::println);
        // the toMap method of Collectors class maps the two functions which 
        // return string values.
    }

}
