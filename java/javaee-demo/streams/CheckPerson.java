/*
13. Consider the following Person:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Check if the above person is in the roster list obtained 
    from Person class.

-----------------------------------------------WBS---------------------------------------------

1.Requirement:
    - Check if the above person is in the roster list obtained from Person class.

2.Entity:
    - CheckPersonInList

3.Function declaration
	- public static void main(String[] args)
	- public static boolean contains(List<Person> list, Person o)

4.Jobs to be done:
    1.Invoke Person class createRoster method and store it in roster List.
    2.Create new Person and store it in aPerson list.
    3.Invoke CheckPerson class contains method with passing list and new person store variable.
    4.For each person
          4.1)Check if person's name, gender, email, birthday using equals method return true else false.

Psudeo Code:
''''''''''''
public class CheckPerson {
    public static boolean contains(List<Person> list, Person o) {
        for (Person person : list) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person aPerson = new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster: " +
                contains(roster, aPerson));
    }
}
-----------------------------------------------Program---------------------------------------------
*/


import java.time.chrono.IsoChronology;
import java.util.List;

public class CheckPerson {

    public static boolean contains(List<Person> list, Person o) {
        for (Person person : list) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person aPerson = new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster: " +
                contains(roster, aPerson));
    }
}
