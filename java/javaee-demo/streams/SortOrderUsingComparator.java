/*
9) Sort the roster list based on the person's age in descending order
using comparator

------------------------------------WBS---------------------------------------------

1.Requirement:
    - Sort the roster list based on the person's age in descending order using comparator

2.Entity:
    - SortOrderUsingComparator

3.Function declaration
    - public static void main(String[] args)

4.Jobs to be done:
    1.Invoke Person class createRoster method and store it in personList List.
    2.Sort with person age get using Person class getAge method and reverse 
using comparator and reversed method.
    3.For each personList print using Person class printPerson method.

Psudeo Code:
''''''''''''
public class SortOrderUsingComparator {
    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.sort(Comparator.comparing(Person::getAge).reversed());
        for (Person person : personList) {
            person.printPerson();
        }
    }
}
------------------------------------Program----------------------------------------
*/


import java.util.Comparator;
import java.util.List;

public class SortOrderUsingComparator {

    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.sort(Comparator.comparing(Person::getAge).reversed());
        // sorting the list using Comparator methods
        for (Person person : personList) {
            person.printPerson();
        }
        // iterating the list to print the person details
    }

}
