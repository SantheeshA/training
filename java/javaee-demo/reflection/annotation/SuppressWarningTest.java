package annotation;

/*	
Problem Statement:
2. 
   i)Deprecate the display method.
   ii)How to suppress the deprecate message.
   
class DeprecatedTestTest 
{   
    //method to be deprecated
    public void Display() 
    { 
        System.out.println("Deprecatedtest display()"); 
    } 
} 
  
public class Test 
{ 
 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 
----------------------------------------WBS-----------------------------------------
1.Requirement:
   - Program to display method and to suppress the deprecate message. 

2.Entity:
   - DeprecatedTest
   - Test

3.Method Declaration:
   - public void display() 
   - public static void main(String args[])

Jobs to be done:
    
    1. Create a class DeprecatedTest with display method.
    2. Using Deprecated annotation make the display method as deprecated method.
    3. method fails to execute and throws an error message as " SuppressWarningTest.java uses or overrides a deprecated API."
    4. To suppress the deprecated error, use SuppressWarning annotation to suppress "checked" and "deprecation" error in main class.
    5. The deprecation is suppressed and output is displayed.

Pseudo code:
''''''''''''
class DeprecatedTest 
{   
    @Deprecated
    public void Display() 
    { 
        System.out.println("Deprecatedtest display()"); 
    } 
} 
  
public class SuppressWarningTest
{ 
    @SuppressWarnings({"checked", "deprecation"}) 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 
----------------------------------------Program Code-----------------------------------------
*/

class DeprecatedTest { 
    @Deprecated
    public void Display() 
    { 
        System.out.println("Deprecatedtest display()"); 
    } 
} 
public class SuppressWarningTest 
{ 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 
