package annotation;
/*

ANNOTATIONS:
============
Problem Statement:
1)USE OVERRIDE ANNOTATION and complete the program,
SHOW THE OUTPUT OF THIS PROGRAM: 

class Base 
{ 
     public void display() 
     { 
         
     } 
} 
class Derived extends Base 
{ 
     public void display(int x) 
     { 
         
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}
----------------------------------------WBS-----------------------------------------
1.Requirment:
    - Program to override annotation and show the output,

2.Entity:
    - Base
    - Derived

Function Declaration:
    - public void display() 
    - public void display(int x) 
    - public static void main(String args[])

4.Jobs to be done:
   1.Create a parent class Base.
   2.Create a Derived class extends Base.
   3.Using override annotation override the Derived class Display method
	
Pseudo Code:
''''''''''''
class Base{
    public void display()
	{
    }
}
class Derived Extends Base{
    @Override
    public void display(int x)
	{
	public static void main(String args[]) 
    { 
        Derived obj = new Derived(); 
        obj.display(); 
    } 

}
----------------------------------------Program Code-----------------------------------------
*/

class Base 
{ 
     public void display() 
     { 
         
     }
     	 
} 
class Derived extends Base 
{ 

    // public void display(int x) 
     { 
         
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}
