package arrays;

/*
Problem Statement:
Program java array using reflect.array class and add elements using the appropriate method.

-----------------------------------WBS------------------------------------------
1.Requirment:
    - Program java array using reflect.array class and add elements using the appropriate method.
 
2.Entity:
    - ArrayAddElement 
   
3.Method Delcaration
    - 
4.Jobs To Be Done:
    1.Stroe the array size in integer variable.
    2.Create integer array using newInstance method.
    3.Add element to array using setInt method.
  
 Pseudo Code:
 ''''''''''''
 public class ArrayAddElement { 
   public static void main(String[] args) { 
	  int sizeOfArray = 3; 
	  int[] intArray = (int[])Array.newInstance(int.class, sizeOfArray); 
      System.out.println(Arrays.toString(intArray));
    }
} 
-----------------------------------Program Code------------------------------------------
*/
import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayAddElement {
	public static void main(String[] args) {

		int sizeOfArray = 3;
		// Create an integer array using reflect.Array class this is done using the
		// newInstance() method
		int[] intArray = (int[]) Array.newInstance(int.class, sizeOfArray);

		// Add elements into the array and this is done using the setInt() method
		Array.setInt(intArray, 0, 10);
		Array.setInt(intArray, 1, 20);
		Array.setInt(intArray, 2, 30);

		System.out.println(Arrays.toString(intArray));
	}
}
