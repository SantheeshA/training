package constrctor;
/*
Problem Statement:
To Create a constructor of type noargconstructor.
-----------------------------------WBS------------------------------------------
1.Requirements:
   - Create a constructor of type noargconstructor.
2.Entity:
   - Private Constructor
3.Function Declaration:
   - public static void main(String[] args)
4.Jobs to be done:
   1. Create an No Argument Constructor.
   2. Display the output.
-----------------------------------Program Code------------------------------------------
*/

class Main {

	int i;
	private static Main obj;

	// constructor with no parameter
	private Main() {
		i = 5;
		System.out.println("Object created and i = " + i);
	}

	public static void main(String[] args) {

		setObj(new Main());
	}

	public static Main getObj() {
		return obj;
	}

	public static void setObj(Main obj) {
		Main.obj = obj;
	}
}
