/*6)Handle and give the reason for the exception in the following code: 
        PROGRAM:
             public class Exception {  
             public static void main(String[] args)
             {
  	       int arr[] ={1,2,3,4,5};
	      System.out.println(arr[7]);
            }
          }
//Display the output.
--------------------------------------WBS------------------------------------------------
1.Requirement:
   - Handle and give the reason for the exception in the following code      
  Display the output.
2.Entity:
  - Exception
3.Functon declaration:
  - public static void main(String[] args)
4.Jobs to be done:
   1.Declare and Initialize the array value in try block.
   2.When an exception occurs in try block ,then go to the catch block.
   3.Check the exception, which are known to run time.
   4.Display the output.

Psudeo Code:
'''''''''''
public class ExceptionDemo {  
   public static void main(String[] args) {
	try{
	   int arr[] ={1,2,3,4,5};
	   System.out.println(arr[7]);
	}
        catch(ArrayIndexOutOfBoundsException e){
	   System.out.println("The specified index does not exist " +
		"in array");
	}
   }
}
------------------------------------------Correct Program-------------------------------------*/


public class ExceptionDemo {  
   public static void main(String[] args) {
	try{
	   int arr[] ={1,2,3,4,5};
	   System.out.println(arr[7]);
	}
        catch(ArrayIndexOutOfBoundsException e){
	   System.out.println("The specified index does not exist " +
		"in array");
	}
   }
}

/*---------------Output---------------
   The specified index does not exist in array
   
   
---------------------Reason---------------
    Array has only five elements but we are trying to display the value of 8th element.
It should throw ArrayIndexOutOfBoundsException which is a unchecked exception.*/

