/*Write a program ListOfNumbers (using try and catch block).
 -----------------------------------------WBS---------------------------------------
   Requirement:
   - Write a program ListOfNumbers (using try and catch block).
 Entity:
   - ListOfNumbers
   - MainList
 Function Declaration:
   - public void writeList()
   - public static void main(String[] args)
 Jobs to be done:
   1.Create an object for ListOfNumbers class
   2.Invoke ListOfNumbers class writeList method
      2.1)Try block initialise array value
      2.2)Catch blocks print error using getMessage method.
               2.2.1)NumberFormatException
               2.2.2)IndexOutOfBoundsException 

Psudeo Code:
''''''''''''
class ListOfNumbers {
  public int[] arrayOfNumbers = new int[10];
  public void writeList() {
    try {
      arrayOfNumbers[10] = 5;
    } catch (NumberFormatException e1) {
      System.out.println("NumberFormatException => " + e1.getMessage());
    } catch (IndexOutOfBoundsException e2) {
      System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
    }
  }
  public static void main(String[] args) {
	  ListOfNumbers list = new ListOfNumbers();
      list.writeList();
  }
}

-----------------------------------------Program---------------------------------------          
 */


class ListOfNumbers {
  public int[] arrayOfNumbers = new int[10];
  public void writeList() {
    try {
      arrayOfNumbers[10] = 5;
    } catch (NumberFormatException e1) {
      System.out.println("NumberFormatException => " + e1.getMessage());
    } catch (IndexOutOfBoundsException e2) {
      System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
    }
  }
  public static void main(String[] args) {
	  ListOfNumbers list = new ListOfNumbers();
      list.writeList();
  }
}
