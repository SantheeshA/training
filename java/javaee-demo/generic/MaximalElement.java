/* 4. Write a generic method to find the maximal element in the range [begin, end) of a list.
 
 --------------------------------WBS--------------------------------------
1.Requirement:
    To find the maximum element of the given program using generic method.

2.Entity:
    public class MaximalElement

3.Function Declaration:
    public static void main(String[] args)
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    
4.Jobs to be Done:
    1.Create Array list with pass elements and invoke MaximalElement class max method.
            1.1)Compare the elements and find the maximum between the range and return it.

Psudeo Code:
''''''''''''
public class MaximalElement {
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
        T maximumElement = list.get(begin);
        for (++begin; begin < end; ++begin) {
            if (maximumElement.compareTo(list.get(begin)) < 0) {
                maximumElement = list.get(begin);
            }
        }   
        return maximumElement;
    }
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(max(list, 3, 6));
    }
}
---------------------------------------Program--------------------------------
*/

import java.util.Arrays;
import java.util.List;

public class MaximalElement {
    
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
        T maximumElement = list.get(begin);
        for (++begin; begin < end; ++begin) {
            if (maximumElement.compareTo(list.get(begin)) < 0) {
                maximumElement = list.get(begin);
            }
        }   
        return maximumElement;
    }
    
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(max(list, 3, 6));
    }
}
