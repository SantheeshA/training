/*Generics-Methods and Class:
 ---*----*----*----*----*----
 2.Will the following class compile? If not, why?
   	public final class Algorithm {
           public static <T> T max(T x, T y) {
               return x > y ? x : y;
           }
       }

Answer
''''''
    The code does not compile, because the greater than (>) operator applies only to primitive numeric types.
 
*/
