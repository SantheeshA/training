/*Generics-Class literals:
1. Write a program to demonstrate generics - class objects as type literals.
  --------------------------------WBS--------------------------------------
1.Requirement:
    - Write a program to demonstrate generics - class objects as type literals.
2.Entity:
    - public class Dog

3.Function Declaration:
    - public static void main(String[] args)
    - public void sound()
    - public static <T> boolean checkInterface(Class<?> theClass)
    
4.Jobs to be Done:
    1.Create literal class and invoke Cat class checkInterface method. 
         1.1)Print type literal class,getClass method to class name and getName method to type class.
    2.Create literal class and invoke Cat class checkInterface method. 
         2.1)Print type literal class,getClass method to class name and getName method to type class.
    3.Create literal class and invoke Cat class checkInterface method. 
         3.1)Print type literal class,getClass method to class name and getName method to type class.
    4.Using TryCatch exception handling Checking class , getClass and getName present or not.if not catch error and displays.
    
Psudeo Code:
''''''''''''
interface Animal {
	public void sound();
}

public class Cat implements Animal {
    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }
    public void sound() {
        System.out.println("Meow");
    }
    public static void main(String[] args) {
        Class<Integer> intClass = int.class;            
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1);                   
        System.out.println(intClass.getClass());        
        System.out.println(intClass.getName());         
        boolean boolean2 = checkInterface(Cat.class);
        System.out.println(boolean2);                   
        System.out.println(Cat.class.getClass());       
        System.out.println(Cat.class.getName());        
        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3);                   
        System.out.println(Animal.class.getClass());    
        System.out.println(Animal.class.getName());     
        try {
            Class<?> errClass = Class.forName("Cat");
            System.out.println(errClass.getClass());
            System.out.println(errClass.getName());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }
} 
---------------------------------------Program--------------------------------
*/

interface Animal {
	public void sound();
}

public class Cat implements Animal {

    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void sound() {
        System.out.println("Meow");
    }

    public static void main(String[] args) {
    	// Creating the int class 
        Class<Integer> intClass = int.class;            
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1);                   
        System.out.println(intClass.getClass());        
        System.out.println(intClass.getName());         

        //Creating Cat class using checkInterface() 
        boolean boolean2 = checkInterface(Cat.class);
        System.out.println(boolean2);                   
        System.out.println(Cat.class.getClass());       
        System.out.println(Cat.class.getName());        

        //Creating Animal class checkInterface()
        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3);                   
        System.out.println(Animal.class.getClass());    
        System.out.println(Animal.class.getName());     

        try {
            Class<?> errClass = Class.forName("Cat");
            System.out.println(errClass.getClass());
            System.out.println(errClass.getName());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }
}
/*
---------Output------------
false
class java.lang.Class
int
false
class java.lang.Class
Cat
true
class java.lang.Class
Animal
class java.lang.Class
Cat
*/