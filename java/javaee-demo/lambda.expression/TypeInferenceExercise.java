/*
 What's wrong with the following program? And fix it using Type Reference

interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(int 23,int 32);
        
        System.out.println(print);
    }
}
----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Convert the following anonymous class into lambda expression.
2.Entities
   - TypeInferenceExercise
   - BiFunction (Interface)
3.Function Declaration
   - int print(int number1, int number2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Declare the single method with two integer parameters.
   2.Create interface object and assign with passing integer values returning the addition of two values.
   3.Invoke the interface single method with integer value and return the value using assigned
interface object lambda expression.

Psudeo Code:
interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (number1, number2) -> number1 + number2; // Don't want to specify type and single brackets { } for single statement
        int print = function.print(23,32); // Don't want to specify type while passing value
        System.out.println(print);
    }
}

-----------------------------------------------Program--------------------------------------------------------------------------------*/ 
interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (number1, number2) -> number1 + number2; // Don't want to specify type and single brackets { } for single statement
        int print = function.print(23,32); // Don't want to specify type while passing value
        System.out.println(print);
    }
}