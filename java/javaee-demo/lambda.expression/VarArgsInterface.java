/* 
 Code a functional program that can return the sum of elements of varArgs, passed into the 
method of functional interface.

  ----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Code a functional program that can return the sum of elements of varArgs, passed into the  method of functional interface.
2.Entities
   - VarArgsInterface
   - MultipleValues (Interface)
3.Function Declaration
   - int sum(int... numbers);
   - public static void main(String[] args)
4.Jobs to be done
   1.Declare the single method with multiple integer parameters using varargs.
   2.Create interface object and assign with passing integer values returning the addition of two values.
   3.Invoking the interface single method with multiple integer value and return all added value using assigned
interface object lambda expression.

Psudeo Code:

interface MultipleValues {
	int sum(int... numbers);
}
public class VarArgsInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MultipleValues multipleValues = (int... numbers) -> {
		int sum = 0;
		for (int i : numbers)  
		    sum += i;  
		  return sum; 
		};
		System.out.println(multipleValues.sum(2,5,6,3,8));
	}
}


-----------------------------------------------Program--------------------------------------------------------------------------------*/


interface MultipleValues {
	int sum(int... numbers);
}
public class VarArgsInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MultipleValues multipleValues = (int... numbers) -> {
		int sum = 0;
		for (int i : numbers)  
		    sum += i;  
		  return sum; 
		};
		System.out.println(multipleValues.sum(2,5,6,3,8));
	}

}
