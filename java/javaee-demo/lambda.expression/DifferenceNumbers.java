/*
Write a program to print difference of two numbers using lambda expression and the single method interface

----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Program to print difference of two numbers using lambda expression and the single method interface
2.Entities
   - DifferenceNumbers
   - Different (Interface)
3.Function Declaration
   - int numbers(int number1,int number2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Declare the single method with two integer parameters.
   2.Create interface object and assign the lambda expression to return two integers difference.
   3.Invoking the interface single method with integer values and return 
the value using assigned interface object lambda expression .

Psudeo Code:
''''''''''''
interface Different {
	int numbers(int number1,int number2);
}

public class DifferenceNumbers {
	public static void main(String[] args) {
		Different different = (number1,number2) -> number1 - number2;
		System.out.println(different.numbers(15,7));
	}
}

-----------------------------------------------Program--------------------------------------------------------------------------------*/


interface Different {
	int numbers(int number1,int number2);
}

public class DifferenceNumbers {
	public static void main(String[] args) {
		Different different = (number1,number2) -> number1 - number2;
		System.out.println(different.numbers(15,7));
	}
}
