/*  
 Write a program to print the volume of a Rectangle using lambda expression.

  ----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Program to print the volume of a Rectangle using lambda expression.
2.Entities
   - VolumeRectangle
   - PrismValues (Interface)
3.Function Declaration
   - int Values(int base,int length,int height);
   - public static void main(String[] args)
4.Jobs to be done
   1.Declare the single method with integer parameters.
   2.Create interface object and assign with passing prism values returning prism value.
   3.Invoking the interface single method with multiple integer value and return value using assigned
interface object lambda expression.


Psudeo Code:
'''''''''''
interface PrismValues {
	int Values(int base,int length,int height);
}
public class VolumeRectangle {
	public static void main(String[] args) {
		PrismValues prismValues = (base,length,height) -> base * length * height;
		System.out.println(prismValues.Values(5,8,16));
	}
}
-----------------------------------------------Program--------------------------------------------------------------------------------*/

interface PrismValues {
	int Values(int base,int length,int height);
}


public class VolumeRectangle {

	public static void main(String[] args) {
		PrismValues prismValues = (base,length,height) -> base * length * height;
		System.out.println(prismValues.Values(5,8,16));

	}

}
