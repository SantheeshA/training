/*Convert the following anonymous class into lambda expression.
interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}
----------------------------------------Word Breakdown Structure(WBS)--------------------------------------------------------
1.Requirements
   - Convert the following anonymous class into lambda expression.
2.Entities
   - EvenOrOdd
   - CheckNumber (Interface)
3.Function Declaration
   - int numbers(int number1,int number2);
   - public static void main(String[] args)
4.Jobs to be done
   1.Declare the single method with integer variable value parameters
   2.Create interface object and assign with passing int value.
        2.1)Check if mod of 2 is equal to 0 return true . 
        2.2)Else return true outside if block return false.
   3.Invoke the interface single method with integer value and return the value using assigned
interface object lambda expression statements.


Psudeo Code:
''''''''''''
interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = (value) -> { // Convert into Java Lambda Expression 
                if (value % 2 == 0) {
                    return true;
                } else return false;
            };
        System.out.println(number.isEven(11));
    }
}

-----------------------------------------------Program---------------------------------------------------*/ 	

interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = (value) -> { // Convert into Java Lambda Expression 
                if (value % 2 == 0) {
                    return true;
                } else return false;
            };
        System.out.println(number.isEven(11));
    }
}