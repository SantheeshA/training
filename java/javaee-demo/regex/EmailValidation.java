/*
Quantifiers
===========
Problem Statement:
   2.Write a program for email-validation?
	
------------------------------WBS----------------------------------------

1.Requirement :
	- Program for email-validation.

2.Entities :
	- EmailValidation

3.Function declaration:
    - public static void main(String[] args)

3.Job to be Done :
	1.Create string mail and store mail in it.
	2.Pattern class compile validation using pattern method and store it in pattern.
	3.Matcher class mathcer mail using matcher method and store it in matcher.
	4.Check mathcer using find method
	     4.1)Print mail valid or not.
Psudeo Code:
''''''''''''
public class EmailValidation {
    public static void main(String[] args) {
        String mail = "kpriet@gmail.com";
        Pattern pattern = Pattern.compile("[a-z]{6}[@]");
        Matcher matcher = pattern.matcher(mail);   
        if(matcher.find()){
            System.out.println(mail + " is valid mail id");
        }
        else {
            System.out.println(mail + " is not valid mail id");
        }
    }
}

-----------------------------Program----------------------------------------
*/


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
	 
    public static void main(String[] args) {
        
        String mail = "kpriet@gmail.com";
        Pattern pattern = Pattern.compile("[a-z]{6}[@]");
        Matcher matcher = pattern.matcher(mail);   
        if(matcher.find()){
            System.out.println(mail + " is valid mail id");
        }
        else {
            System.out.println(mail + " is not valid mail id");
        }
    }
}
