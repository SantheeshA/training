/*
Logical Operators
=================

Problem Statement:

1.Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
	The discount rate is 10% for the quantity purchased between 100 and 120 units, 
	and 15% for the quantity purchased greater than 120 units. 
	If the quantity purchased is less than 100 units, the discount rate is 0%. 
	See the example output as shown below:
	Enter unit price: 25
	Enter quantity: 110
	The revenue from sale: 2475.0$
	After discount: 275.0$(10.0%)
		  
----------------------------------WBS------------------------------

1.Requirement :
	Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
    The discount rate is 10% for the quantity purchased between 100 and 120 units, 
	and 15% for the quantity purchased greater than 120 units. 
	If the quantity purchased is less than 100 units, the discount rate is 0%. 
	See the example output as shown below:
	Enter unit price: 25
	Enter quantity: 110
	The revenue from sale: 2475.0$
	After discount: 275.0$(10.0%)
	
2.Entities :
	- LogicalOperator
	
3.Method Signature :
    - public static void main(String[] args)
	- public static void calculateSale()
	
3.Job to be Done:
    1.Invoke calculateSale method
    2.Create four float and one int value store value as zero.
    3.Get the input from user for unitprice and quantity.
	4.Check quantity is less than 100
	    3.1)Multiply unitprice and quantity store it in revenue.
	5.Check quantity is greater than or equal to 100 and quantity is less than or equal to 120
	    5.1)Store discount_rate as 10 by 100 type float.
	    5.2)Multiply unitprice and quantity store it in revenue.
	    5.3)Multiply revenue and discount_rate store it in discount_amount.
	    5.4)Subract the revenue with discount_amount and store it in revenue.
	6.Check quantity is greater than or equal to 120. 
	    6.1)Store discount_rate as 15 by 100 type float.
	    6.2)Multiply unitprice and quantity store it in revenue.
	    6.3)Multiply revenue and discount_rate store it in discount_amount.
	    6.4)Subract the revenue with discount_amount and store it in revenue.
	6.Print the revenue and discount_amount

Psudeo Code:
''''''''''''
public class LogicalOperator {
	public static void main(String[] args) {
		calculateSale();
	}
	public static void calculateSale() {
		float unitprice=0f;
		int quantity=0;
		float revenue=0f;
		float discount_rate=0f;
		float discount_amount=0f;
		Scanner scanner = new Scanner(System.in);
		unitprice = scanner.nextFloat();
		quantity = scanner.nextInt();
		if(quantity<100)
			revenue = unitprice*quantity;
		else if(quantity>=100 && quantity<=120)
		{
			discount_rate = (float)10/100;
			revenue = unitprice*quantity;
			discount_amount = revenue*discount_rate;
			revenue -= discount_amount;
		}
		else if(quantity>120) {
			discount_rate = (float)15/100;
			revenue = unitprice*quantity;
			discount_amount = revenue*discount_rate;
			revenue -= discount_amount;
		}
		System.out.println("The revenue from sale:"+revenue+"$");
		System.out.println("After discount:"+discount_amount+"$("+discount_rate*100+"%)");
		}
}
*/


import java.util.Scanner;

public class LogicalOperator {
	public static void main(String[] args) {
		calculateSale();
	}

	public static void calculateSale(){

		float unitprice=0f;
		int quantity=0;
		float revenue=0f;
		float discount_rate=0f;
		float discount_amount=0f;

		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter unit price:");
		unitprice = scanner.nextFloat();
		System.out.print("Enter quantity:");
		quantity = scanner.nextInt();

		if(quantity<100)
			revenue = unitprice*quantity;
		else if(quantity>=100 && quantity<=120)
		{
			discount_rate = (float)10/100;
			revenue = unitprice*quantity;
			discount_amount = revenue*discount_rate;
			revenue -= discount_amount;
		}
		else if(quantity>120) {
			discount_rate = (float)15/100;
			revenue = unitprice*quantity;
			discount_amount = revenue*discount_rate;
			revenue -= discount_amount;
		}
		System.out.println("The revenue from sale:"+revenue+"$");
		System.out.println("After discount:"+discount_amount+"$("+discount_rate*100+"%)");
		}
}
