/*
Quantifiers
=========== 
Problem Statement: 
   1.Create a pattern for password which contains
	   8 to 15 characters in length
	   Must have at least one uppercase letter
	   Must have at least one lower case letter
	   Must have at least one digit

-----------------------------WBS------------------------------------
1.Requirement :
	 - Create a pattern for password which contains
	     8 to 15 characters in length
	     Must have at least one uppercase letter
	     Must have at least one lower case letter
	     Must have at least one digit

2.Entities :
	- PasswordValidation
	
3.Method Signature:
    - public static void main(String[] args)

3.Job to be Done :
	1.Create string password and store password String in it .
	2.Using pattern class 
          2.1)Check the password condition in compile method.
	3.Using matcher class
	      3.1)Check pattern class matches the password.
	4.Check password condition using find method and print valid or not.
	
Psudeo Code:
''''''''''''
public class UserLogin {
    public static void main(String[] args) {
        String password = "KprietCSE123";
        Pattern pattern = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,15}");
        Matcher matcher = pattern.matcher(password);   
        if(matcher.find()){
            System.out.println(password + " is valid Password");
        }
        else {
            System.out.println(password + " is not valid Password");
        }
    }
}
-----------------------------Program Code------------------------------------
*/


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserLogin {
	 
    public static void main(String[] args) {
        
        String password = "KprietCSE123";
        Pattern pattern = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,15}");
        Matcher matcher = pattern.matcher(password);   
        if(matcher.find()){
            System.out.println(password + " is valid Password");
        }
        else {
            System.out.println(password + " is not valid Password");
        }
    }
}
