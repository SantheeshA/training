/*
Regex Methods:
=============
Problem Statement:

1. Write a program for Java String Regex Methods?
	Regex Method :
		1.matches()
		2.spilt()
		3.replaceFirst()
		4.replaceAll()

----------------------------------WBS--------------------------------------------
1.Requirement :
	- Program for Java String Regex Methods.

2.Entities :
	- StringRegMethod

3.Method Signature:
    - public static void main(String[] args)
    
3.Job to be Done :
	1.Create text type String and store a string in it.
	     1.1)Using matches method check string given regular expression true or false.
	2.Create stringSplit type String and store a string in it. 
	     2.2)Using split method split "-" in string.
	     2.3)Print the string using for each.
	3.Create stringReplace type String and store a string in it.
	     3.1)Using replaceFirst method replace the first match string in stringReplace and store it in replaceFirst.
	     3.2)Print replacefirst string.
	4.Create stringReplaceAll type String and store a string in it.
	     4.1)Using replaceAll method replace all the string in stringReplaceAll and store it in replaceAll.
	     
	     
Psudeo Code:
'''''''''''
        String text = "one two"; 
        System.out.println(text.matches(".*two"));  
        String stringSplit = "Welcome-to-FullStack-Training";
        for (String display: stringSplit.split("-")) 
             System.out.print(display+" ");
        System.out.println();
        String stringReplace = "one two three two one"; 
        String replaceFirst = stringReplace.replaceFirst("two", "five"); 
        System.out.println(replaceFirst); 
        String stringReplaceAll = "one two three two one";
        String replaceAll = stringReplaceAll.replaceAll("two", "five");   
        System.out.println(replaceAll);
----------------------------------Program Code--------------------------------------------

*/

public class RegexMethod {
     
    public static void main(String[] args) {
              
        // matches()
        String text = "one two"; 
        System.out.println("Using matches() method :-");
        System.out.println(text.matches(".*two"));  
        
        // spilt()
        System.out.println("\n Using split() method :-");
        String stringSplit = "Welcome-to-FullStack-Training";
        for (String display: stringSplit.split("-")) {
             System.out.print(display+" ");
        }  
        System.out.println();
        
        // replaceFirst()
        System.out.println("\nUsing replaceFirst() method :-");
        String stringReplace = "one two three two one"; 
        String replaceFirst = stringReplace.replaceFirst("two", "five"); 
        System.out.println(replaceFirst); 
       
        // replaceAll() 
        System.out.println("\nUsing replaceAll() method :-");
        String stringReplaceAll = "one two three two one";
        String replaceAll = stringReplaceAll.replaceAll("two", "five");   
        System.out.println(replaceAll);
         
    }
}