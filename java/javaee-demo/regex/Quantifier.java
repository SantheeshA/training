/*
Quantifiers:
============
Problem Statement:
  1.Write a program for java regex quantifer?

	  
---------------------------WBS----------------------------------
1.Requirement :
	- Program for Java String Regex quantifer.

2.Entities :
	- Quantifier
	
3.Job to be Done:
	1.Create sentence type String and store a pattern string in it.
	2.Using pattern class
	        2.1)Store the quantifier patter in compile method it in pattern.
	3.Using matcher class 
            3.1)Check pattern class matches the sentance store it in matcher.
	4.Print the character and index using while loop 
	        4.1)Check condition using find method.

Psudeo Code:
''''''''''''
public class Quantifier {
    public static void main(String[] args) { 
        String sentence = "abaabbaaabbbaaaabab";
        Pattern pattern = Pattern.compile("a*");
        Matcher matcher = pattern.matcher(sentence);   
        while(matcher.find()){
            System.out.println(matcher.group()+" \tstarts at "+matcher.start());
        }    
    }
}
----------------------------Program Code-----------------------------------
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class Quantifier {
     
    public static void main(String[] args) { 
    
        String sentence = "abaabbaaabbbaaaabab";
        Pattern pattern = Pattern.compile("a*");
        Matcher matcher = pattern.matcher(sentence);   
        while(matcher.find()){
            System.out.println(matcher.group()+" \tstarts at "+matcher.start());
        }    
    }
}
