/*
3.Demonstrate linked hash set to array() method in java
 
----------------------------------------------WBS--------------------------------------------------
1.Requirements:
   - Linked hash set to array() method in java
2.Entities
   - LinkedHashSetToArray
3.Function Declaration
   - public static void main(String [] args)
4.Jobs to be done
   1.Create a set as cars 
   2.Add 6 values and remove on value from set using remove() method.
   3.Using .toArray() method covert to array.
   4.Print the Array using for loop.
   
Psudeo Code:
''''''''''''
public class LinkedHashSetToArray {
	public static void main(String [] args) {
        Set<String> cars = new LinkedHashSet<>();
        ADD 5 ELEMENTS
	    System.out.println("The LinkedHashSet: \n"+cars); 
        Object[] car = cars.toArray();  
        System.out.println("The Array elements:");
        for (int i = 0; i < car.length; i++) { 
            System.out.println(car[i]); 
        }	
	}
}
 
 ----------------------------------------------Program--------------------------------------------------*/
import java.util.LinkedHashSet;
import java.util.Set;
public class LinkedHashSetToArray {
	public static void main(String [] args) {
        Set<String> cars = new LinkedHashSet<>();
	    cars.add("Mazda");     //Adding 6 Values
	    cars.add("Lexus");
	    cars.add("Audi");
	    cars.add("BMW");   
	    cars.add("Mazda");
	    cars.add("Mecleran"); 
	    System.out.println("The LinkedHashSet: \n"+cars); //Print the set values
	    
	    System.out.println("------------------------*-------------*------------------*------------------");
	    //Create array convert to elements using .toArray() method
        Object[] car = cars.toArray();  
        System.out.println("The Array elements:");
        for (int i = 0; i < car.length; i++) { 
            System.out.println(car[i]); 
        }	
	}
}
