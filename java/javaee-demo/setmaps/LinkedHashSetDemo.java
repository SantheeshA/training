/*
2.Demonstrate program explaining basic add and traversal operation of linked hash set

----------------------------------------------WBS--------------------------------------------------
1.Requirements:
   - Demonstrate program explaining basic add and traversal operation of linked hash set
2.Entities
    - LinkedHashSetDemo
3.Function Declaration
   - public static void main(String [] args)
4.Jobs to be done
 1.Create a set as cars and add 6 values and remove on value from set.
 2.Create iterator to traverse over cars.
 3.Printing all the set elements using Iterator with hasnext() method.
 
 
Psudeo Code:
''''''''''''
public class LinkedHashSetDemo {
	public static void main(String [] args) {
        Set<String> cars = new LinkedHashSet<>();	
        ADD 5 ELEMENTS 
	    Iterator<String> setIterator = cars.iterator();
        while(setIterator.hasNext()){

             System.out.println(setIterator.next());
          }  		
	}
}
 ----------------------------------------------Program--------------------------------------------------*/
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
public class LinkedHashSetDemo {
	public static void main(String [] args) {
        Set<String> cars = new LinkedHashSet<>();	//Creating a LinkedHashSet 
	    cars.add("Mazda");     //Adding 6 Values
	    cars.add("Lexus");
	    cars.add("Audi");
	    cars.add("BMW");   
	    cars.add("Mazda");
	    cars.add("Mecleran");  
        //creating iterator to traverse over cars
	    Iterator<String> setIterator = cars.iterator();
        while(setIterator.hasNext()){
             System.out.println(setIterator.next());
          }  		
	}
}