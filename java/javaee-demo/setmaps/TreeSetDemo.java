/*
 4.Java program to demonstrate insertions and string buffer in tree set.

----------------------------------------------WBS--------------------------------------------------
1.Requirements:
   - Java program to demonstrate insertions and string buffer in tree set
Entities
   - TreeSetDemo
Function Declaration
   - public int compare(StringBuffer s1, StringBuffer s2)
   - public static void main(String[] args)
Jobs to be done
 1.Create a Class as TreeSetDemo.
 2.Create public int compare with 2 StringBuffers as parameter and return compare two Strings.
 4.Declare main method and create a set as car as String and cars as StringBuffer.
 5.Add 2 values in car and Add 2 values in cars. 
 6.Print the car and cars.
 
----------------------------------------------Program--------------------------------------------------*/
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
public class TreeSetDemo implements Comparator<StringBuffer> {
	
    public int compare(StringBuffer string1, StringBuffer string2) { 
        return string1.toString().compareTo(string2.toString()); //Compare  
    } 
    public static void main(String[] args) {   
    	Set<String> car = new TreeSet<String>();  //Creating set as String
        Set<StringBuffer> cars = new TreeSet<>(new TreeSetDemo()); //Creating set as StringBuffer
        car.add("BMW"); 
        car.add("Meclaren"); 
        System.out.println(car); 
        cars.add(new StringBuffer("Telsa")); 
        cars.add(new StringBuffer("Honda")); 
        System.out.println(cars); 
    } 

}

