/*
1.Java program to demonstrate adding elements, displaying, removing, and iterating in hash set

----------------------------------------------WBS--------------------------------------------------
1.Requirements:
    - Java program to demonstrate adding elements, displaying, removing, and iterating in hash set
2.Entities
   - HashSetDemo
3.Function Declaration
   - public static void main(String [] args)
4.Jobs to be done
 1.Create a set as cars and add 5 values and remove on value from set.
 2.Displaying all the set elements using Iterator 
 
Psudeo Code:
'''''''''''
public class HashSetDemo {
	public static void main(String [] args) {
		Set<String> cars = new HashSet<>();
		ADD 5 ELEMENTS 
	    cars.remove("Tesla");
        Iterator<String> car = cars.iterator();   
        System.out.println("Printing all the set elements using Iterator :");
        while (car.hasNext()) {
            System.out.println(car.next());
        }  	    
	}
}
----------------------------------------------Program--------------------------------------------------*/
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
	public static void main(String [] args) {
		Set<String> cars = new HashSet<>();
        cars.add("Tesla"); 	//Add 5 values in the set
	    cars.add("Honda");
	    cars.add("BMW");
	    cars.add("Audi");
	    cars.add("Mecleran");
	    cars.remove("Tesla");//Remove a value in set
        Iterator<String> car = cars.iterator();  //Printing all the set elements using Iterator 
        System.out.println("Printing all the set elements using Iterator :");
        while (car.hasNext()) {
            System.out.println(car.next());
        }  	    
	}
}
