/*
  Problem Statement
  1.To Test All possible conditions for Address Service 
  
  Requirement
  1.To Test All possible conditions for Address Service
  
  Entity
  1.ServicesTest
  2.PersonService
  3.AppException
  4.Connection
  5.Person
  6.Address
  
  Method Signature
  1.public void setUp()
  2.public void insertAddress()
  3.public void readAddress()
  4.public void readAllAddress()
  5.public void updateAddress()
  6.public void deleteAddress()
  7.public void deleteNotUsedAddress()
  8.public void connectionClose()
  
  Jobs to be done:
  ->public void setUp()
    1.Prepare address object of type Address (pass values).
    2.Prepare addressService object of type AddressService.
  
  @Test for insertAddress
  1.Invoke addressService insertAddress method (pass address object) 
and store in insertAddressStatus.
  2.AssertJUnit assertTrue method (pass Check insertAddressStatus is greater than 0 ).
  
  @Test for readAddress
  1.Invoke addressService readAddress method (pass address id) and
store in List<Address> objectList .
  2.AssertJUnit assertTrue method (pass Check objectList is not equal to null ).
  
  @Test for readAddress
  1.Invoke addressService readAllAddress method and
store in List<Address> objectList.
  2.AssertJUnit assertTrue method (pass Check objectList is not equal to null ).
  
  @Test for updateAddress
  1.Invoke addressService updateAddress method (pass address object) 
and store in udpateStatus.
  2.AssertJUnit assertTrue method (pass Check udpateStatus is not equal to false ).
  
  @Test for deleteAddress
  1.Invoke addressService deleteAddress method (pass address id) 
and store in deleteStatus.
  2.AssertJUnit assertTrue method (pass Check deleteStatus is not equal to false ).

Psudeo Code:
public class AddressServiceTest {
	long id;
	long addressUpdateId = 1;
	Address updatedAddress;
	Address addressRead;
	Address address1;
	Address address2;
	Address addressUpdate;
	AddressService addressService;
	Address deleteAddress;
	ConnectionService connectionService;
	Connection connection;
	
	@BeforeClass
	public void setUp() throws Exception {
		connectionService = new ConnectionService();
		connection = connectionService.initConnection();
		addressRead = new Address("Perumal", "Kanchipuram" , 601200);
		address1 = new Address("OMR", "Chennai" , 600028);
		address2 = new Address("Murugan", "Tiruchendhur", 641600);
		addressUpdate = new Address("AnnaNagar", "Tirupr" , 600000);
		addressService = new AddressService();
	}
	
	@Test(priority = 1, description = "Insert Address")
	public void insertAddress() throws AppException, SQLException {
		this.id = addressService.insertAddress(address1 , connection);
		Assert.assertTrue(this.id > 0);
	}

	@Test(priority = 2, description = "Read Address")
	public void readAddress() throws AppException, SQLException {
		System.out.println(this.id);
		address2 = addressService.readAddress(this.id, connection);
		address1.id = this.id;
		Assert.assertEquals(address2.toString(), address1.toString());
	}
	@Test(priority = 3, description = "Read All Address")
	public void readAllAddress() throws AppException, SQLException {
		Assert.assertTrue(addressService.readAllAddress(connection).toString() != null);
	}
	@Test(priority = 4, description = "Update Address")
	public void updateAddress() throws AppException, SQLException {
		addressService.updateAddress(addressUpdate, addressUpdateId, connection);
		updatedAddress = addressService.readAddress(addressUpdateId, connection);
		addressUpdate.id = addressUpdateId;
		Assert.assertEquals(updatedAddress.toString(), addressUpdate.toString());
	}
	@Test(priority = 5, description = "Delete Address")
	public void deleteAddress() throws AppException, SQLException {
		addressService.deleteAddress(this.id, connection);
	}
	
	@Test(priority = 6, description = "Delete Unused Address")
	public void deleteNotUsedAddress() throws AppException, SQLException {
		long deleteNotUsedAddress = addressService.deleteNotUsedAddress(connection);
		System.out.println(deleteNotUsedAddress);
		Assert.assertEquals(1 , deleteNotUsedAddress);
	}
	
	@AfterClass
	public void connectionClose() {
		try {
			connectionService.releaseConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
*/

package com.kpr.training.jdbc.test;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.service.AddressService;
import com.kpr.training.jdbc.service.ConnectionService;

public class AddressServiceTest {

	AddressService addressService = new AddressService();
	Address address1;
	Address address2;
	
	
	long actualAddressCreatedId;
	long expectedAddressCreatedId;
	Address actualAddressRead;
	Address expectedAddressRead;
	List<Address> actualAddressReadAll = new ArrayList<Address>();
	int expectedAddressReadAll;
	Address actualAddressUpdate;
	Address expectedAddressUpdate;
	long addressDeleteId;
	Address expectedAddressDelete;
	Address expectedDeleteUnusedAddress;
	Address expectedSearchAddress;
	long expectedDeleteNotUsedAddressId;
	long deleteNotUsedAddress;
	long addressUpdateId = 1;
	Address updatedAddress;
	
	
	@BeforeClass
	public void setUp() throws Exception {
		expectedAddressRead = new Address("Perumal", "Kanchipuram" , 601200);
		address1 = new Address("OMR", "Chennai" , 600028);
		address2 = new Address("Murugan", "Tiruchendhur", 641600);
		actualAddressUpdate = new Address("AnnaNagar", "Tirupr" , 600000);
	}
	
	@Test(priority = 1, description = "Insert Address")
	public void insertAddress() throws Exception {
		long id = addressService.create(new Address("T Nagar", "Chennai", 600028));
		System.out.println(id);
		Assert.assertNotNull(id);
		
		if(id != 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
		ConnectionService.release();
	}

	@Test(priority = 2, description = "Read Address")
	public void readAddress() throws Exception {
		actualAddressRead = addressService.read(actualAddressCreatedId);
		expectedAddressRead.id = actualAddressRead.id;
		Assert.assertEquals(actualAddressRead.toString(), expectedAddressRead.toString());
	}
	@Test(priority = 3, description = "Read All Address")
	public void readAllAddress() throws Exception {
		actualAddressReadAll = addressService.readAll();
		Assert.assertEquals(actualAddressReadAll.size(), expectedAddressReadAll);
	}
	@Test(priority = 4, description = "Update Address")
	public void updateAddress() throws Exception {
		addressService.update(actualAddressUpdate, addressUpdateId);
		expectedAddressUpdate = addressService.read(addressUpdateId);
		Assert.assertEquals(expectedAddressUpdate.toString(), actualAddressUpdate.toString());
	}
	@Test(priority = 5, description = "Delete Address")
	public void deleteAddress() throws Exception {
		addressService.delete(addressDeleteId);
	}
	
	@Test(priority = 6, description = "Delete Unused Address")
	public void deleteNotUsedAddress() throws Exception {
		addressService.deleteNotUsedAddress();
		Assert.assertEquals(null, addressService.read(deleteNotUsedAddress));
	}
	
	@Test(priority = 7, description = "Search Address")
	public void searchPersonAdress() {
		AddressService addressService = new AddressService();
		expectedSearchAddress = new Address(null,"Tirupur", 0);
		List<Address> addressList = addressService.search(expectedSearchAddress);
		Assert.assertNotNull(Stream.of(addressList.toString()));
	}
}
