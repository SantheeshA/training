package com.kpr.training.jdbc.exception;

public class AppException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public AppException(ErrorCode error, Exception e) {
		super(error.getMessage(), e);
	}
	
	public AppException(ErrorCode error) {
		super(error.getMessage());
	}
}
