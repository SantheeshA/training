package com.kpr.training.jdbc.constant;

public class QueryStatement {
	
	public static final String CREATE_ADDRESS_QUERY          = new StringBuffer("INSERT INTO address     ")
			                                                            .append("           (street      ")
			                                                            .append("           ,city        ")
			                                                            .append("           ,postal_code)")
			                                                            .append("     VALUES(?,?,?)      ")
			                                                            .toString();
	                                                         
	                                                         
	public static final String UPDATE_ADDRESS_QUERY          = new StringBuffer("UPDATE address         ")
                                                                        .append("   SET street = ?      ") 
                                                                        .append("      ,city = ?        ")
                                                                        .append("      ,postal_code = ? ") 
                                                                        .append(" WHERE id = ?          ")
                                                                        .toString();
	                                                         
	public static final String READ_ADDRESS_QUERY            = new StringBuilder("SELECT id          ")
                                                                         .append("      ,street      ") 
                                                                         .append("      ,city        ")
                                                                         .append("      ,postal_code ")
                                                                         .append("  FROM address     ")
                                                                         .append(" WHERE id = ?      ")
                                                                         .toString(); 
	                                                         
	public static final String READ_ALL_ADDRESS_QUERY        = new StringBuilder("SELECT id          ")
                                                                         .append("      ,street      ")
                                                                         .append("      ,city        ")
                                                                         .append("      ,postal_code ")
                                                                         .append(" FROM  address     ")
                                                                         .toString();
	                                                         
	public static final String DELETE_NOT_USED_ADDRESS_QUERY = new StringBuilder("SELECT id          ")
                                                                         .append("  FROM address     ")
                                                                         .append(" WHERE id          ")
                                                                         .append("NOT IN             ")
                                                                         .append("(SELECT address_id ")
                                                                         .append("   FROM person )   ")
                                                                         .toString();
	
	public static final String ADDRESS_SEARCH_QUERY          = new StringBuilder("SELECT id          ")
			                                                             .append("      ,street      ")
			                                                             .append("      ,city        ")
			                                                             .append("      ,postal_code ")
			                                                             .append("  FROM address     ")
			                                                             .append(" WHERE street      ")
			                                                             .append("  LIKE ?           ")
			                                                             .append("    OR city        ")
			                                                             .append("  LIKE ?           ")
			                                                             .append("    OR postal_code ")
			                                                             .append("  LIKE ?           ")
			                                                             .toString();
	
	public static final String DELETE_ADDRESS_QUERY          =  new StringBuffer("DELETE FROM address ")
			                                                             .append("      WHERE id = ?  ")
			                                                             .toString();
	
	public static final String CREATE_PERSON_QUERY           =  new StringBuffer("INSERT INTO person      ")
			                                                             .append("           (first_name  ") 
			                                                             .append("           ,last_name   ")
			                                                             .append("           ,email       ")
			                                                             .append("           ,address_id  ")
			                                                             .append("           ,birth_date) ")
			                                                             .append("    VALUES (?,?,?,?,?)  ")
			                                                             .toString();
	
	public static final String UPDATE_PERSON_QUERY           =  new StringBuffer("UPDATE person         ")
			                                                             .append("   SET first_name = ? ")
			                                                             .append("      ,last_name = ?  ")
			                                                             .append("      ,email = ?      ")
			                                                             .append("      ,address_id = ? ")
			                                                             .append("      ,birth_date = ? ")
			                                                             .append(" WHERE id = ?         ")
			                                                             .toString();
	
	public static final String READ_PERSON_QUERY             =  new StringBuffer("SELECT id           ")
			                                                             .append("      ,first_name   ")
			                                                             .append("      ,last_name    ")
			                                                             .append("      ,email        ")
			                                                             .append("      ,address_id   ")
			                                                             .append("      ,birth_date   ")
                                                                         .append(" FROM person        ")
                                                                         .append("WHERE id = ?        ")
                                                                         .toString(); 
	
	public static final String READ_ALL_PERSONS_QUERY        = new StringBuffer("SELECT id          ")
			                                                            .append("      ,first_name  ")  
                                                                        .append("      ,last_name   ")
			                                                            .append("      ,email       ") 
			                                                            .append("      ,address_id  ")
			                                                            .append("      ,birth_date  ")
                                                                        .append("  FROM person      ")
                                                                        .toString();
	
	public static final String DELETE_PERSON_QUERY           = new StringBuffer("DELETE FROM person ")
                                                                        .append("      WHERE id = ? ")
                                                                        .toString(); 
	
	public static final String GET_ADDRESS_ID_QUERY          = new StringBuffer("SELECT id              ")
                                                                        .append("  FROM address         ")
                                                                        .append(" WHERE street = ?      ")
                                                                        .append("   AND city = ?        ")
                                                                        .append("   AND postal_code = ? ")
                                                                        .toString();
	
	public static final String CHECK_UNIQUE_NAME_QUERY       = new StringBuilder("SELECT id            ")
											                            .append("  FROM person         ")
											                            .append(" WHERE first_name = ? ")
											                            .append("   AND last_name = ?  ")
											                            .append("HAVING id != ?        ")
											                            .toString();
	
	public static final String GET_PERSON_ID_QUERY           = new StringBuilder("SELECT id      ")
			                                                           .append("  FROM person    ")
			                                                           .append(" WHERE email = ? ")
			                                                           .append("HAVING id != ?   ")
			                                                           .toString();
	public static final String GET_ADDRESS_ID_COUNT          = new StringBuilder("SELECT COUNT(id) AS id_count            ")
													                   .append("                    , address_id         ")
													                   .append("  FROM person                          ")
													                   .append(" WHERE address_id = (SELECT address_id ")
													                   .append("                       FROM person     ")
													                   .append("                      WHERE id = ?)    ")
													                   .toString();            
}
                            
