package com.kpr.training.jdbc.validation;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Person;

public class PersonValidator {
	public static void isNameNotNull(Person person) throws Exception {

		if (person.getFirstName() == null || person.getLastName() == null || person.getFirstName() == " "
				|| person.getLastName() == " " || person.getFirstName() == "" || person.getLastName() == "") {
			throw new AppException(ErrorCode.ERR17);
		}
		if (person.getBirthDate() == null) {
			throw new AppException(ErrorCode.ERR18);
		}
	}

}
