import java.util.Scanner; // importing Scanner library for getting user inputs

import com.training.java.core.Person;

public class WithPackage{ 
       public static void main(String args[]) { 
        Scanner scanner = new Scanner(System.in); // creating object for getting user inputs
        System.out.println("Enter Employee Id,Name");
        Person person = new Person("1CS92","BalaKumaran");//Creating Object for Child Class
        /* employeeId,employeeName,dateOfBirth are parent properties getting inputs from user using
         child(inherited) class */
        person.employeeId = scanner.nextLine(); //Getting user input for only employeeId 
        person.printInfo(); // calling methods in child class
        /*employeeId is public so easily accessible in any packages and classes 
        and employeeName is protected so it is accessible in same packages and inherited classes
        and dateOfBirth is private so it is accessible in defined class or within a class*/
    }
}

/*Output
EmployeeID: 12CS90 EmployeeName: null
employeeName is not not getting inputs from user in the above class because it is protected
property is accessible only  in inherited class and same packages.The default value of String is
null because user not initializing any values.
dateOfBirth is also not getting inputs from user in the above class because it is private
property is accessible only in definied class or within a class
*/