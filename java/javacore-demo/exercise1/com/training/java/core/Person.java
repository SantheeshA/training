package com.training.java.core; // creating package


public class Person extends Employee{ //Inherits Employee Parent class
    public String employeeId; // Defining employeeId property as public
    protected String employeeName; //Defining employeeName property as protected
    public Person(String employeeId,String employeeName) // Parameterized Constructor 
    {
        super(employeeId,employeeName);// super keyword refers to parent objects 
    }
    public void printInfo() { // Method Declaration 
        System.out.println("EmployeeID: "+ this.employeeId +" "+ "EmployeeName: " + this.employeeName);
        /*Printing only public and protected properties because public can be accessible in all classes,
        then protected only accessible in same packages and inherited classes*/
    }
}
