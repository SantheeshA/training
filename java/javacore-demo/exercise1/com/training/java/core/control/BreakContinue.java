package com.training.java.core.control;

public class BreakContinue {
    public void Break() {
        for (int i = 0; i < 10; i++) {
            if (i == 4) {
                break;
            }
            System.out.println(i);
        }
    }
    public void Continue() {
        for (int i = 0; i < 10; i++) {
            if (i == 4) {
                continue;
            }
            System.out.println(i);
        }
    }
    public static void main(String[] args){
        BreakContinue breakcontinue = new BreakContinue();
        System.out.println("Using Break.....");
        breakcontinue.Break();
        System.out.println("Using Continue.....");
        breakcontinue.Continue();
    }
    
}