package com.training.java.core; //creating package

public class Employee {// creating a class as Employee
    public String employeeId; // Defining employeeId property as public 
    protected String employeeName; //Defining employeeName property as protected
    public Employee(String employeeId,String employeeName) {  // Parameterized Constructor 
        this.employeeId = employeeId;   // Initalising property from user inputs
        this.employeeName = employeeName; //this keyword is used to refers the current object

    }
}