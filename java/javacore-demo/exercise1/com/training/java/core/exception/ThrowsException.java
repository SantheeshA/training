package com.training.java.core.exception;
import java.io.*;


class ThrowsDemo { 
  void myMethod(int num)throws IOException, ClassNotFoundException{ 
     if(num == 1)
        throw new IOException("IOException Occurred");
     else
        throw new ClassNotFoundException("ClassNotFoundException");
  } 
} 

public class ThrowsException { 
  public static void main(String args[]){ 
   try{ 
     ThrowsDemo obj = new ThrowsDemo(); 
     obj.myMethod(1); 
   }catch(Exception ex){
     System.out.println(ex);
    } 
  }
}