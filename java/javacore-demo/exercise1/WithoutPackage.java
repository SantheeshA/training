class Employee{
    //Access Specifiers 
    public String employeeId;//Property Declaration
    private String employeeName; 
    protected String dateOfBirth;
    public Employee(String empId,String empName,String empdob) { //Parameterized Constructor 
        employeeId = empId;
        employeeName = empName;
        dateOfBirth = empdob;
    }
    public void printInfo() {  // Methods Declaration
        System.out.println("EmployeeName: " + employeeName);  //private employeeName is not acceessible in another class
        EmployeeDept empdept = new EmployeeDept("Computer Science");// Creating Object for Inner Class
        System.out.println("EmployeeDept: " + empdept.employeeDept); //Prints employeeDept
    }
    class EmployeeDept{  // Inner Class 
        protected String employeeDept;  
        /* protected empDept property is accessible only in same packages and inheritance */
        public EmployeeDept(String empDept) {
            employeeDept = empDept;
        }
        public void display() {
            System.out.println("EmpDept: " + employeeDept); 
        }
    }
}
class WithoutPackage {
    public static void main(String args[]) {
        Employee emp = new Employee("1CS92","BalaKumaran","12-09-2000"); // Passing Constructor Arguments
        emp.printInfo(); /* To display Private Properties Using Methods */
        System.out.println("EmployeeId: " + emp.employeeId); //Print employeeId: "1CS92"
        System.out.println("Employeedob: " + emp.dateOfBirth); // Print empdob: "12-09-2000"
    }
}
