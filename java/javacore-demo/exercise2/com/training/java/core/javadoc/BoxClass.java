package com.training.java.core.javadoc;

/*Use the Java API documentation for the Box class (in the javax.swing package) to help you answer 
the following questions.
    - What static nested class does Box define?
    - What inner class does Box define?
    - What is the superclass of Box's inner class?
    - Which of Box's nested classes can you use from any class?
    - How do you create an instance of Box's Filler class?


What static nested class does Box define?
   Answer: Box.Filler

What inner class does Box define?
   Answer: Box.AccessibleBox

What is the superclass of Box's inner class?
   Answer: [java.awt.]Container.AccessibleAWTContainer

Which of Box's nested classes can you use from any class?
   Answer: Box.Filler

How do you create an instance of Box's Filler class?
   Answer: new Box.Filler(minDimension, prefDimension, maxDimension)*/