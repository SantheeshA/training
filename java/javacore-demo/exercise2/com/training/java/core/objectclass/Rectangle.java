package com.training.java.core.objectclass;
//What's wrong with the following program? And fix it.

/*public class SomethingIsWrong {
    public static void main(String[] args) {
        Rectangle myRect;
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
        }
    }*/
/*-----Word Breakdown Structure(WBS)------
1.Requirments
        Rectangle myRect;
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area(myRect.width,myRect.height)); // calling method with two arguments
        }
2.Entities
    -Rectangle
3.Function Declaration
    -public int area(int width,int height)
    -public static void main(String[] args)
4.Jobs to be done
    1.Declare instance variable inside the class.
    2.Declare integer method with two parameters for return the calculated area value and declare the main method.
    3.Create an object for Rectangle class and initialiing values for two variables using object.
    4.Print and invoking a Rectangle class area method with passing two arguments using object.*/
    
    
//Fixed Code.
public class Rectangle { //Class name changed as Rectangle
    public int width; //Initialized two instances variables width and height
    public int height;
    public int area(int width,int height) { // Declared method in the given datatype with parameters
        return width*height; // returning rectangle calculated value 
    }
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle(); // creating an myRect object 
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area(myRect.width,myRect.height)); // calling method with two arguments
        }
    }
    
//-------Output--------
// myRect's area is 2000