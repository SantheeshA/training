set path = C:\Program Files\Java\jdk\bin

javac BooleanFlag.java
javac BooleanInverter.java
javac CmdLineFloatAdder.java
javac CmdLineIntAdder.java
javac ExpressionType.java
javac PrimitiveClassNames.java
javac WrapperOverload.java


java BooleanFlag
java BooleanInverter
java CmdLineFloatAdder
java CmdLineIntAdder
java ExpressionType
java PrimitiveClassNames
java WrapperOverload

pause