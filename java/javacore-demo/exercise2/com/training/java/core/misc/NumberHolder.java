package com.training.java.core.misc;

/*Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.
----------Word Breakdown Structure(WBS)--------
1.Requirments
    Creates an instance of the class and initializes its two member variables with provided values,
and then displays the value of each member variable.
2.Entities
    -NumberHolder
3.Function Declaration
    -public void display()
    -public static void main(String args[])
4.Jobs to be done
    1.Create an class called NumberHolder.
    2.Declare two instance variable inside the class.
    3.Inside the class declare method with two parameters and declare main method.
    4.In the main method create an object and initializing values to two integer and float variables
using object
    5.Invoke the method with two parameters float and integer.*/


public class NumberHolder {
    public int anInt;
    public float aFloat;
    public void display(int anInt,float aFloat) {
        System.out.println("anInt = " + anInt +" "+ "aFloat = " + aFloat);
    }
    public static void main(String args[]) {
        NumberHolder numHold = new NumberHolder();
        numHold.anInt = 25;
        numHold.aFloat = 7.5f;
        numHold.display(numHold.anInt,numHold.aFloat);
    }
}

/*Output
anInt = 25 aFloat = 7.5*/