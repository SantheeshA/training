package com.training.java.core.inheritance;
/*
Consider the following two classes:
public class ParentClass {
    public void methodOne(int number) {
    }
    public void methodTwo(int number) {
    }
    public static void methodThree(int number) {
    }
    public static void methodFour(int number) {
    }
}

public class ChildClass extends ParentClass {
    public static void methodOne(int number) {
    }
    public void methodTwo(int number) {
    }
    public void methodThree(int number) {
    }
    public static void methodFour(int i) {
    }
}
------------------Word Breakdown Structure(WBS)--------------------
1.Requirments
    -ParentClass
    -ChildClass
2.Entities
    -ParentClass
    -ChildClass
3.Function Declaration
    -public void methodOne(int number)
    -public static void methodOne(int number)
    -public void methodTwo(int number)
    -public void methodThree(int number)
    -public static void methodThree(int number)
    -public static void methodFour(int number)


a.Which method overrides a method in the superclass?
   - methodTwo

b.Which method hides a method in the superclass?
   - methodFour

c.What do the other methods do?
   - They cause compile-time errors.*/



