package com.training.java.core.interfac;

/*What is wrong with the following interface? and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }
----------Word Breakdown Structure(WBS)----------

1.Requirement:
    -What is wrong with the following interface and fix it.
2.Entity:
    -No Entity
3.Function Declaration:
    -default void aMethod(int aValue);
Jobs to be done:
     1.Create an interface called FixedCode with default method aMethod with integer parameter
and print the statement.
*/

/*What is wrong with the following interface?
   It has a method implementation in it. Only default and static methods have
implementations.*/

//Fixed code.
public interface InterfaceFixedCode {
    default void aMethod(int aValue) {
        System.out.println("Hi Mom");
    }
}