package com.training.java.core.interfac;
/*

 Is the following interface valid?
    public interface Marker {}

Answer : 
      Yes. The above following interface is valid.Methods are not required because empty 
interfaces can be used as types and to mark classes without requiring any particular method 
implementations.*/