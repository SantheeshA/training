package com.training.java.core.operators;

/*Change the following program to use compound assignments:

----------------Word Breakdown Structure(WBS)-------------------------
1.Requirments
    -Program to use compound assignments.
2.Entities
    -ArithmeticDemo
3.Function Declaration
    -public static void main (String[] args)
4.Jobs to be done
    1.Declare the integer variable number and initialize the value .
    2.Using compound assignment adding,subracting,multiplying and dividing with integer value.
    3.Each initialisation and calculation print the value.*/

public class ArithmeticDemo {
    public static void main (String[] args){
        int number = 1;
        number += 2; // number is now 3
        System.out.println(number);
        
        number -=  1; // number is now 2
        System.out.println(number);
        
        number *= 2; // number is now 4
        System.out.println(number);
        
        number /= 2; // number is now 2
        System.out.println(number);
        
        number += 8; // number is now 10
        System.out.println(number);
        
        number %= 7; // number is now 3
        System.out.println(number);
     }
}
