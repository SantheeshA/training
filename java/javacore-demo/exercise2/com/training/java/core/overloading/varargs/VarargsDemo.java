package com.training.java.core.overloading.varargs;

/*Demonstrate overloading with varArgs
----------------Word Breakdown Structure(WBS)-------------------------
1.Requirments
    -Demonstrate overloading with varArgs.
2.Entities
    -VarargsDemo
3.Function Declaration
    -public static void Numbers(int... numbers)
    -public static void main (String[] args)
    -public static void Words(String... string)
    -public static void DecimalNumber(double... numbers)
4.Jobs to be done
    1.Invoke the VarargsDemo class static
          1.1)Numbers method prints length of array and all integer values using Varargs 
          1.2)Word method prints length of array and all String values using Varargs
          1.3)DecimalNumber method prints length of array and all double values using Varargs
    3.All type of array prints using far each loop*/


public class VarargsDemo {
   public static void Numbers(int... numbers) {
      System.out.println("\nNumber of int arguments are: " + numbers.length);
      System.out.println("The int argument values are: ");
      for (int i : numbers)
      System.out.println(i);
   }
   public static void Words(String... string) {
      System.out.println("\nNumber of string arguments are: " + string.length);
      System.out.println("The string argument values are: ");
      for (String i : string)
      System.out.println(i);
   }
   public static void DecimalNumber(double... numbers) {
      System.out.println("\nNumber of double arguments are: " + numbers.length);
      System.out.println("The double argument values are: ");
      for (double i : numbers)
      System.out.println(i);
   }
   public static void main(String args[]) {
      Numbers(4, 9, 1, 6, 3);
      Words("Happy", "Enjoy", "WorkHard");
      DecimalNumber(5.9, 2.5);
   }
}