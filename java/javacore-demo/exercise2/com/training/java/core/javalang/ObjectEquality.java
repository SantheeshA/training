package com.training.java.core.javalang;

/*Demonstrate object equality using Object.equals() vs ==, using String objects
 -------------Word Breakdown Structure(WBS)------------------
1.Requirments
    -Demonstrate object equality using Object.equals() vs ==, using String objects.
2.Entities
    -ObjectEquality
3.Function Declaration
    -public static void main( String[] args )
    -equals()
4.Jobs to be done
    1.Create a two String object s1 and s2, passing a string arguments values. 
    2.Compare two string using 
        2.1)equal == operator
        2.2)equals() method
 */
public class ObjectEquality { 
    public static void main(String[] args) 
    { 
        String s1 = new String("HELLO"); 
        String s2 = new String("HELLO"); 
        System.out.println(s1 == s2); 
        System.out.println(s1.equals(s2)); 
    } 
} 

/*Demonstrate object equality using Object.equals() vs ==, using String objects

When using == operator for s1 and s2 comparison then the result is false as both have 
different addresses in memory.
Using equals, the result is true because its only comparing the values given in s1 and s2.

-------Output------
false
true */