package com.training.java.core.string;
/*

How long is the string returned by the following expression? What is the string?
"Was it a car or a cat I saw?".substring(9, 12)
        Answer: 
              It's 3 characters in length: car. It does not include the space after car.

*/