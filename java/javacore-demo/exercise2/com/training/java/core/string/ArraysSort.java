package com.training.java.core.string;

/*sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }*/
/*----------Word Breakdown Structure(WBS)----------

1.Requirement:
    -Sort and print following array alphabetically ignoring case.
    -Also convert and print even indexed Strings into uppercase.

2.Entity:
    -Sort
3.Function Declaration:
    -No Function
Jobs to be done:
    1.Declare the String array variable and initialize array value.
    2.Using buildin array sort method and prints sorted array using for loop.
    3.Using for loop initialize value , check the length of name and increment variable.
and inside the loop check even condition to convert to uppercase of String.
    4.Print the sorted and an even indexed array.

*/
import java.util.Arrays;
class ArraysSort {
    public static void main(String[] args) {
        String[] districts = { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort( districts, 0, districts.length );
        for (String i : districts) {
            System.out.print(i + " ");
        }
        System.out.println("\nEven indexed districts:");
        for(int i = 0; i < districts.length; i++) {
            if(i % 2 == 0) {
                String district = districts[i].toUpperCase();
                System.out.print(district + " ");
            }
        }
    }
}

