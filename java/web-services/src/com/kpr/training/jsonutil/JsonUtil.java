package com.kpr.training.jsonutil;

import com.google.gson.GsonBuilder;
import com.kpr.training.jdbc.constant.Constant;

public class JsonUtil {

	public static Object stringToObject(String jsonString, Class<?> type) {
		return new GsonBuilder().setDateFormat(Constant.DATE_FORMATE).create().fromJson(jsonString, type);
	}

	public static String objectToString(Object objectClass) {
		return new GsonBuilder().setDateFormat(Constant.DATE_FORMATE).create().toJson(objectClass);
	}
}