package com.kpr.training.jdbc.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.service.AuthenticationService;
import com.kpr.training.jdbc.validation.UserLoginValidation;

public class AuthenticationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	String token = "3u3rb83r9end9fhyf82ybd8hfh9r39fndomljsjxbhfus";

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

		try (PrintWriter writer = res.getWriter()) {

			if (new AuthenticationService().checkUserPassword(req.getParameter("userName"),
					req.getParameter("password"))) {
				writer.append("User Succefully Login " + token);
			} else {
				throw new AppException(ErrorCode.ERR45);
			}
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
