package com.kpr.training.jdbc.servlet;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.service.AddressService;
import com.kpr.training.jsonutil.JsonUtil;

public class AddressServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) {

		try {

			long addressId = new AddressService().create((Address) JsonUtil.stringToObject(new StringBuffer()
					.append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator()))).toString(),
					Address.class));

			res.getWriter().append("Address Creation Success : " + JsonUtil.objectToString(addressId));
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) {

		long addressId = Long.parseLong(req.getParameter("id"));
		try {
			if (addressId == 0) {
				res.getWriter().append(JsonUtil.objectToString(new AddressService().readAll()));
			} else {
				res.getWriter().append(JsonUtil.objectToString(new AddressService().read(addressId)));
			}
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPut(HttpServletRequest req, HttpServletResponse res) throws IOException {

		try {

			new AddressService().update((Address) JsonUtil.stringToObject(new StringBuffer()
					.append(req.getReader().lines().collect(Collectors.joining(System.lineSeparator()))).toString()
					.toString(), Address.class), Long.parseLong(req.getParameter("id")));
			res.getWriter().append("Person Update Successfully");
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws IOException {

		try {
			new AddressService().delete(Long.parseLong(req.getParameter("id")));
			res.getWriter().append("Address Delete Successfully");
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
