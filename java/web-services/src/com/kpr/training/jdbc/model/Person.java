/*---------------------------------------WBS---------------------------------------


Entity
   1.Person

Jobs To be Done:
   1.Create a class with Person variables.
   2.Create constructor to Address class for assign vlaues.
         2.1)Person.id variable and Person.created_date are an auto generated.
   3.Create getter and setter methods for private variables.

Psudeo Code:
''''''''''''

public class Person {
	public long id;
	private String firstName;
	private String lastName;
	private String email;
	private Date birthDate;
	private Date createdDate;
	private Address address;
	
    //Getter & Setter
      
    //toString method 
}
 */

package com.kpr.training.jdbc.model;

import java.sql.Date;

public class Person {

	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private Date birthDate;
	private Date createdDate;
	private Address address;
	private User user;

	public Person(long id, String firstName, String lastName, String email) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public Person(long id, String firstName, String lastName, String email, Date birthDate) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDate = birthDate;
	}

	// Read Person Constructor
	public Person(long id, String firstName, String lastName, String email, Address address, Date birthDate) {

		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.birthDate = birthDate;
	}

	// Insert Person Constructor
	public Person(String firstName, String lastName, String email, Address address, Date birthDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.birthDate = birthDate;
	}

	// Read Person Constructor
	public Person(long id, String firstName, String lastName, String email, Address address, Date birthDate,
			Date createdDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.birthDate = birthDate;
		this.createdDate = createdDate;
	}

	// Insert Person Constructor
	public Person(String firstName, String lastName, String email, User user, Address address, Date birthDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.user = user;
		this.address = address;
		this.birthDate = birthDate;
	}

	public Person() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public long getAddressId() {
		return address.getId();
	}

	public void setAddressId(long id) {
		address.setId(id);
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public User getUser() {
		return user;
	}

	public void setsSer(User user) {
		this.user = user;
	}

	public long getUserId() {
		return user.getId();
	}

	public void setId(int id) {
		user.setId(id);
	}

	public String getUserRole() {
		return user.getRole();
	}

	public void setUserRole(String role) {
		user.setRole(role);
	}

	public String getUserName() {
		return user.getUserName();
	}

	public void setUserName(String userName) {
		user.setUserName(userName);
	}

	public String getUserPassword() {
		return user.getPassword();
	}

	public void setUserPassword(String password) {
		user.setPassword(password);
	}

	@Override
	public String toString() {
		return new StringBuilder("Person [id = ").append(id).append(", firstName = ").append(firstName)
				.append(", lastName  = ").append(lastName).append(", email     = ").append(email).append(", Address = ")
				.append(address.toString()).append(", birthDate = ").append(birthDate).append("]").toString();
	}

}
