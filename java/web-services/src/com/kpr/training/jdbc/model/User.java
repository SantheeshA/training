package com.kpr.training.jdbc.model;

public class User {
	private long id;
	private String role;
	private String userName;
	private String password;

	public User(long id, String role, String userName, String password) {
		super();
		this.id = id;
		this.role = role;
		this.userName = userName;
	}
	
	public User(long id, String role, String userName) {
		this.id = id;
		this.role = role;
		this.userName = userName;
	}
	
	public User(String role, String userName, String password) {
		this.role = role;
		this.userName = userName;
		this.password = password;
	}


	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return new StringBuilder("User [id=").append(id).append(", role=").append(role).append(", userName=")
				.append(userName).append(", password=").append(password).toString();
	}
}
