package com.kpr.training.jdbc.validation;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.User;

public class UserValidation {
	public static void validateUser(User user) {

		if (user.getRole() == null || user.getUserName() == null || user.getPassword() == null || user.getRole() == " "
				|| user.getUserName() == " " || user.getPassword() == " ") {
			throw new AppException(ErrorCode.ERR17);
		}
	}
}
