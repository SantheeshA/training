package com.kpr.training.jdbc.validation;

import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;

public class UserLoginValidation {
	public static void isUserPasswordValid(String userName, String password) {
		if (userName == "" || userName == " " || userName == null || password == "" || password == " "
				|| password == null) {
			System.out.println("UserName and Password should not be null or empty");
			throw new AppException(ErrorCode.ERR44);
		}
	}
}
