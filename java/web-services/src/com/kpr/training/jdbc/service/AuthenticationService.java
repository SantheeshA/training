package com.kpr.training.jdbc.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.model.User;
import com.kpr.training.jdbc.validation.UserLoginValidation;

public class AuthenticationService {

	public boolean checkUserPassword(String userName, String password) throws Exception {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.GET_USER_ID_QUERY)) {
			ps.setString(1, userName);
			ps.setString(2, password);
			UserLoginValidation.isUserPasswordValid(userName, password);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
			return false;
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR42, e);
		}

	}
}
