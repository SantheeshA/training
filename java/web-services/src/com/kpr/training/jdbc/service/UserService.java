package com.kpr.training.jdbc.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.User;

public class UserService {

	public long create(User user) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.CREATE_USER_PASSWORD,
				Statement.RETURN_GENERATED_KEYS)) {

			setPs(ps, user);
			
			ResultSet rs;
			if (ps.executeUpdate() == 1 && (rs = ps.getGeneratedKeys()).next()) {
				ConnectionService.commitRollback(true);
				return rs.getLong(Constant.GENERATED_ID);
			}
			ConnectionService.commitRollback(false);
			return 0;
		} catch (AppException e) {
			throw e;
		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR38, e);
		}
	}

	public User read(long id) {

		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_USER_QUERY)) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();

			if (!rs.next()) {
				return null;
			}
			return getRs(rs);

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR39, e);
		}
	}

	public List<User> readAll() {

		List<User> users = new ArrayList<>();
		try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.READ_ALL_PERSONS_QUERY)) {

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				users.add(getRs(rs));
			}
			return users;

		} catch (Exception e) {
			throw new AppException(ErrorCode.ERR40, e);
		}
	}

	public static User getRs(ResultSet rs) throws SQLException {
		return new User(rs.getLong(Constant.ID), rs.getString(Constant.ROLE), rs.getString(Constant.USER_NAME));
	}

	public void setPs(PreparedStatement ps, User user) throws SQLException {
		ps.setString(1, user.getRole());
		ps.setString(2, user.getUserName());
		ps.setString(3, user.getPassword());
	}
	public static void main(String[] args) {
		System.out.println(new UserService().create(new User("student", "Swagy16", "123456")));
		
	}
}
