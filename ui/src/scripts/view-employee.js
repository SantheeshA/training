const loadEmployee = (employeesData) => {
  let employeeDetail = document.getElementById("grid-table");
  employeeDetail.innerHTML = "";
  let columnName = document.createElement("div");
  let columnArea = document.createElement("div");
  let columnID = document.createElement("div");
  let columnAction = document.createElement("div");

  columnID.innerHTML = "ID";
  columnName.innerHTML = "Name";
  columnArea.innerHTML = "Area";
  columnAction.innerHTML = "Actions";

  columnName.classList.add("grid-title");
  columnName.classList.add("grid-items");
  columnArea.classList.add("grid-title");
  columnArea.classList.add("grid-items");
  columnID.classList.add("grid-title");
  columnID.classList.add("grid-items");
  columnAction.classList.add("grid-title");
  columnAction.classList.add("grid-items");

  employeeDetail.appendChild(columnID);
  employeeDetail.appendChild(columnName);
  employeeDetail.appendChild(columnArea);
  employeeDetail.appendChild(columnAction);

  employeesData.forEach((employee) => {
    let id = document.createElement("div");
    let name = document.createElement("div");
    let area = document.createElement("div");
    let actions = document.createElement("div");
    let deleteLink = document.createElement("a");

    name.classList.add("grid-items");
    id.classList.add("grid-items");
    area.classList.add("grid-items");
    actions.classList.add("grid-items");

    id.innerHTML = employee.id;
    name.innerHTML = employee.name;
    area.innerHTML = employee.area;
    deleteLink.innerHTML = "Delete";
    id.onclick = () => setFormEdit(employee);
    deleteLink.onclick = () =>
      remove("http://localhost:3001/employee", employee.id);

    actions.appendChild(deleteLink);
    employeeDetail.appendChild(id);
    employeeDetail.appendChild(name);
    employeeDetail.appendChild(area);
    employeeDetail.appendChild(actions);
  });
};

let addEmployee = () => {
  let employee = {
    id: document.getElementById("id").value,
    name: document.getElementById("name").value,
    area: document.getElementById("area").value,
  };
  create("http://localhost:3001/employee", employee);
};

let setFormEdit = (employee) => {
  document.getElementById("name").value = employee.name;
  document.getElementById("area").value = employee.area;
  document.getElementById("id").value = employee.id;
  document.getElementById("formAddEmployee").innerHTML = "Update";
  document.getElementById("formAddEmployee").onclick = () =>
    update("http://localhost:3001/employee", employee.id);
};
