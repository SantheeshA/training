const read = (url) => {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.onreadystatechange = () => {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      loadEmployee(JSON.parse(xhr.responseText));
    }
  };
  xhr.send();
};

read("http://localhost:3001/employee", () => console.log("Data Successfully Readed"));

const remove = (url, id) => {
  const xhr = new XMLHttpRequest();
  let responseText;
  xhr.open("DELETE", url + `/${id}`, true);
  xhr.onreadystatechange = () => {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      read(url);
    }
  };
  xhr.send();
};

const create = (url, data) => {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
  xhr.onreadystatechange = () => {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      read(url);
    }
  };
  xhr.send(JSON.stringify(data));
};

const update = (url, id) => {
  let data = {};
  data.name = document.getElementById("name").value;
  data.area = document.getElementById("area").value;

  let xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = () => {
    if (xhttp.readyState == XMLHttpRequest.DONE) {
      read(url);
    }
  };
  xhttp.open("PUT", `${url}/${id}`, true);
  xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send(JSON.stringify(data));
};
