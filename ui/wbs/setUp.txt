1.Requirements
   - Configure db.json file with json-server 
2.Jobs To be Done:
   1. After installed json-server create db.json file.
   2. Add data to db.json file in json format.
   

1.Requirements
    -Install Node v14.16 and generate package.json file
    -Install json-server and lite-server 

2.Jobs to be done:
1.Download node v14.16.0 and install in windows
2.Check whether node v14.16.0 installed correctly or not.
   2.1. After installed run npm init in command prompt to generate package.json file
          -npm init
3.Install both json-server and lite-server project module globally using 
     -npm install -g json-server lite-server
4.Install both json-server and lite-server project module path specified project using 
     -npm install json-server --save
     -npm install lite-server --save-dev
5.Check whether json-server and lite-server is correctly installed or not.
6.Change main value as index.html and create index.html file .



1.Requirements
   - Configure lite-server run with localhost:3000 port.

2.Jobs To be Done:
   1. Create index.html and index.js config with package.json.
   2. In index.html create employee student UI with professitional colors.
   3. In index.js create GET and POST http request and response to view the 
db.json data in UI.