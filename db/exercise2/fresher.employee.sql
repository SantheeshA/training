10. Prepare a query to find out fresher(no department allocated employee) in the employee, where no matching records in the department table.

SELECT first_name
      ,surname
      ,dept_no
  FROM employee
 WHERE dept_no IS NULL;