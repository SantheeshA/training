8.Write a query to list out employees from the same area, and from the same department

ALTER TABLE employee 
  ADD COLUMN area VARCHAR(45) NOT NULL AFTER `dept_no`;

UPDATE employee 
   SET area = 'coimbatore' 
WHERE (`emp_id` = '1');
UPDATE employee 
   SET area = 'namakkal'
WHERE (`emp_id` = '2');
UPDATE employee
   SET area = 'kochi' 
WHERE (`emp_id` = '3');
UPDATE employee 
   SET area = 'mumbai' 
WHERE (`emp_id` = '4');
UPDATE employee
   SET area = 'karnataka'
WHERE (`emp_id` = '5');
UPDATE employee 
   SET area = 'coimbatore' 
WHERE (`emp_id` = '10');
UPDATE employee 
   SET area = 'karnataka'
WHERE (`emp_id` = '7');
UPDATE employee
   SET area = 'mumbai'
WHERE (`emp_id` = '6');
UPDATE employee 
   SET area = 'kochi' 
WHERE (`emp_id` = '8');
UPDATE employee 
   SET area = 'namakkal' 
WHERE (`emp_id` = '9');
UPDATE employee 
   SET area = 'mumbai' 
WHERE (`emp_id` = '15');
UPDATE employee 
   SET area = 'coimbatore' 
WHERE (`emp_id` = '13');
UPDATE employee 
   SET area = 'kochi' 
WHERE (`emp_id` = '14');
UPDATE employee 
   SET area = 'karnataka' 
WHERE (`emp_id` = '11');
UPDATE employee 
   SET area = 'namakkal' 
WHERE (`emp_id` = '12');
UPDATE employee 
   SET area = 'karnataka' 
WHERE (`emp_id` = '17');
UPDATE employee 
   SET area = 'mumbai'
WHERE (`emp_id` = '19');
UPDATE employee 
   SET area = 'kochi' 
WHERE (`emp_id` = '18');
UPDATE employee 
   SET area = 'namakkal' 
WHERE (`emp_id` = '16');
UPDATE employee 
   SET area = 'coimbatore'
WHERE (`emp_id` = '20');
UPDATE employee 
   SET area = 'karnataka' 
WHERE (`emp_id` = '21');
UPDATE employee 
   SET area = 'mumbai' 
WHERE (`emp_id` = '22');
UPDATE employee 
   SET area = 'kochi' 
WHERE (`emp_id` = '23');
UPDATE employee 
   SET area = 'namakkal' 
WHERE (`emp_id` = '24');
UPDATE employee 
   SET area = 'coimbatore' 
WHERE (`emp_id` = '25');
UPDATE employee 
   SET area = 'karnataka'
WHERE (`emp_id` = '26');
UPDATE employee 
   SET area = 'namakkal' 
WHERE (`emp_id` = '27');
UPDATE employee 
   SET area = 'kochi' 
WHERE (`emp_id` = '28');
UPDATE employee 
   SET area = 'mumbai' 
WHERE (`emp_id` = '29');
UPDATE employee 
   SET area = 'coimbatore' 
WHERE (`emp_id` = '30');



SELECT employee.first_name
      ,employee.area
  FROM employee 
      ,department 
 WHERE employee.dept_no = department.dept_no 
   AND area = 'kochi' 

SELECT employee.first_name
      ,employee.area
      ,dept_name
  FROM employee 
      ,department 
 WHERE employee.dept_no = department.dept_no 
   AND dept_name = 'ITDesk' 

SELECT area
      ,first_name
  FROM employee
 ORDER BY area;

SELECT employee.first_name 
      ,department.dept_name
  FROM employee 
      ,department 
 WHERE department.dept_no = 202
   AND employee.dept_no = department.dept_no
   AND department.dept_name IN
      (SELECT department.dept_name
         FROM employee
             ,department 
        WHERE employee.dept_no = department.dept_no
          AND department.dept_no = 202);