11. Write a query to get employee names and their respective department name

SELECT employee.first_name
      ,department.dept_name 
  FROM employee 
 INNER JOIN department
   ON (department.dept_no = employee.dept_no)