6. Find out the average salary

SELECT AVG(annual_salary) As Avg_salary
  FROM employee
 WHERE dept_no IN
      (SELECT dept_no 
         FROM department
        WHERE employee.dept_no = department.dept_no);