5. Find out the highest and least paid in all the department

SELECT dept_no
      ,MAX(annual_salary) AS MaxSalary
  FROM employee 
 GROUP BY dept_no


SELECT dept_no
      ,MIN(annual_salary) AS MinSalary 
  FROM employee 
 GROUP BY dept_no
