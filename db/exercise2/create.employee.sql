1. Create tables to hold employee(emp_id, first_name, surname, dob, date_of_joining, annual_salary), possible depts: ITDesk, Finance, Engineering, HR, Recruitment, Facility
    employee number must be the primary key in employee table
    department number must be the primary key in department table
    department number is the foreign key in the employee table
    
CREATE SCHEMA `employee managemnet`

CREATE TABLE `employee managemnet`.`department` (`dept_no` INT NOT NULL
			                                    ,`dept_name` VARCHAR(45) NOT NULL
                                                 ,PRIMARY KEY (`dept_no`));

CREATE TABLE `employee managemnet`.`employee` (`emp_id` INT NOT NULL
                                              ,`first_name` VARCHAR(45) NOT NULL
                                              ,`surname` VARCHAR(45) NOT NULL
                                              ,`dob` DATE NOT NULL
                                              ,`date_of_joining` DATE NOT NULL
                                              ,`annual_salary` INT NOT NULL
                                              ,`dept_no` INT NOT NULL
                                               ,PRIMARY KEY (`emp_id`)
                                               ,CONSTRAINT FK_dept FOREIGN KEY (dept_no)
                                                REFERENCES department(dept_no));
