9. Write a query to find out employee birthday falls on that day

SELECT * 
  FROM employee 
 WHERE DATE_FORMAT(dob,'%d-%m') = DATE_FORMAT(SYSDATE(),'%d-%m');