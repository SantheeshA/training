2. Write a query to insert at least 5 employees for each department

INSERT INTO `employee managemnet`.`department` (`dept_no`
                                               ,`dept_name`) 
VALUES ('201'
INSERT INTO `employee managemnet`.`department` (`dept_no`
                                               ,`dept_name`) 
VALUES ('202'
       ,'Finance');
INSERT INTO `employee managemnet`.`department` (`dept_no`
                                               ,`dept_name`)
VALUES ('203'
       ,'Engineering');
INSERT INTO `employee managemnet`.`department` (`dept_no`
                                               ,`dept_name`)
VALUES ('204' 
       ,'HR');
INSERT INTO `employee managemnet`.`department` (`dept_no`
                                               ,`dept_name`) 
VALUES ('205'
       ,'Recruitment');
INSERT INTO `employee managemnet`.`department` (`dept_no`
                                               ,`dept_name`)
VALUES ('206'
       ,'Facility');


INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (1
      ,'Kailash'
      ,'Rameshavaran'
      ,'1993-08-04'
      ,'2011-02-01'
       ,220000
       ,201);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (2
      ,'Mohamed'
      ,'Appas'
      ,'1980-08-01'
      ,'2017-02-12'
       ,430000
       ,202);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (3
      ,'Suresh'
      ,'Raja'
      ,'2001-09-09'
      ,'2015-08-15'
       ,300000
       ,203);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (4
	  ,'Abisek'
      ,'Kuru'
      ,'2001-09-09'
      ,'2015-08-15'
       ,180000
       ,204);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (5
      ,'Sabari'
      ,'Mathan'
      ,'1990-12-09'
      ,'2016-10-11'
       ,400000
       ,205);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (6
      ,'Ramesh'
      ,'Loganathan'
      ,'1990-09-08'
      ,'2017-09-23'
       ,500000
       ,206);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (7
      ,'Makesh'
      ,'Lokesh'
      ,'1992-01-30'
      ,'2019-11-21'
       ,450000
       ,206);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (8
      ,'Suresh'
      ,'Mukesh'
      ,'1995-09-12'
      ,'2018-12-09'
       ,380000
       ,205);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (9
      ,'Nisanth'
      ,'Setharth'
      ,'1991-08-04'
      ,'2011-11-02'
       ,150000
       ,204);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (10
       ,'Praveen'
       ,'Mahesh'
       ,'1990-01-04'
       ,'2016-11-14'
        ,240000
        ,203);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (11
       ,'Rahul'
       ,'Nirmal'
       ,'1990-09-08'
       ,'2015-12-08'
        ,390000
        ,202);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (12
	  ,'Bhuvanesh'
      ,'Kumaravel'
      ,'1987-02-09'
      ,'2016-01-10'
       ,195000
       ,201);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (13
      ,'Varun'
      ,'Rajan'
      ,'1990-09-12'
      ,'2011-10-02'
       ,180000
       ,201);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (14
      ,'Arul'
      ,'Vasanth'
      ,'1993-07-18'
      ,'2017-06-19'
       ,370000
       ,202);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob` 
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (15
      ,'Murukesh'
      ,'Prasanth'
      ,'1990-01-04'
      ,'2011-10-02'
       ,230000
       ,203);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (16 
      ,'Maruthan'
      ,'Kamesh'
      ,'1990-09-18'
      ,'2016-01-10'
       ,110000
       ,204);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (17
      ,'Rahul'
      ,'Mahesh'
      ,'1995-01-29'
      ,'2015-12-08'
       ,340000
       ,205);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname` 
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (18
      ,'Kevin'
      ,'Kumar'
      ,'1990-02-11'
      ,'2019-10-12'
       ,469000
       ,206);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (19
      ,'Rajesh'
      ,'Cris'
      ,'1990-01-28'
      ,'2019-01-03'
       ,400000
       ,206);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (20
      ,'Vinay'
      ,'Bob'
      ,'1992-01-05'
      ,'2019-11-14'
       ,300000
       ,205);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (21
      ,'Ram'
      ,'Kuru'
      ,'1990-01-08'
      ,'2015-01-09'
       ,100000
       ,204);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (22
      ,'Anish'
      ,'Kumar '
      ,'1992-09-04'
      ,'2016-01-10'
       ,210000
       ,203);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (23
      ,'Rajesh'
      ,'Cris '
      ,'1993-08-17'
      ,'2011-10-02'
       ,312800
       ,202);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (24
      ,'Alex'
      ,'Robert'
      ,'1990-09-28'
      ,'2017-06-09'
       ,207000
       ,201);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (25
      ,'Steve '
      ,'Anup'
      ,'1995-01-09'
      ,'2017-03-18'
       ,170000
       ,201);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (26
      ,'Kevin'
      ,'George'
      ,'1993-08-27'
      ,'2019-12-09'
       ,310000
       ,202);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (27
      ,'Suresh'
      ,'Bob'
      ,'1990-09-12'
      ,'2011-11-02'
       ,430000
       ,203);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`)
VALUES (28
      ,'Joseph'
      ,'Ken'
      ,'1995-02-11'
      ,'2017-03-08'
       ,260000
       ,204);
INSERT INTO `employee managemnet`.`employee` (`emp_id`
                                             ,`first_name`
                                             ,`surname`
                                             ,`dob`
                                             ,`date_of_joining`
                                             ,`annual_salary`
                                             ,`dept_no`) 
VALUES (29
      ,'Shyam'
      ,'Kumar '
      ,'1991-01-09'
      ,'2019-12-29'
       ,580500
       ,205);
INSERT INTO `employee managemnet`.`employee` ( `emp_id`
                                              ,`first_name`
                                              ,`surname`
                                              ,`dob`
                                              ,`date_of_joining`
                                              ,`annual_salary`
                                              ,`dept_no`)
VALUES (30
      ,'Robert'
      ,'Michael'
      ,'1987-11-23'
      ,'2017-12-02'
       ,550000
       ,206);
      ,'ITDesk');
