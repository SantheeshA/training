3. Select students details who are studying under a particular university
and selected cities alone. Fetch 20 records for every run.
roll_number, name, gender, dob, email, phone, address,
COLLEGE_name, DEPARTMENT_name, hod_name

ALTER TABLE university ADD INDEX(university_name);

SELECT stud.roll_number
      ,stud.name
      ,stud.gender
      ,stud.dob
      ,stud.email
      ,stud.phone
      ,stud.address
      ,coll.id
      ,dept.dept_name
      ,emp.name AS hod_name
  FROM college coll 
 INNER JOIN university univ
    ON coll.univ_name = univ.univ_name
 INNER JOIN employee emp
    ON coll.id = emp.college_id
 INNER JOIN designation desig
    ON emp.desig_id = desig.id
 INNER JOIN college_department coll_dept 
    ON emp.cdept_id = coll_dept.cdept_id
 INNER JOIN department dept
    ON coll_dept.udept_code = dept.dept_code
 INNER JOIN student stud
    ON coll_dept.cdept_id = stud.cdept_id
 WHERE univ.university_name = "Anna University" 
   AND coll.city = "Coimbatore"
   AND desig.name = 'HOD'
 LIMIT 20