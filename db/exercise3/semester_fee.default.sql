6. Create new entries in semester_FEE table for each student from all
the colleges and across all the universities. These entries should be
created whenever new semester starts.. Each entry should have below
default values;
a) amount - Semester fees
b) paid_year - Null
c) paid_status - Unpaid

ALTER TABLE `university management`.`semester_fee` 
CHANGE COLUMN `amount` `amount` DOUBLE NULL DEFAULT 35000 ,
CHANGE COLUMN `paid_year` `paid_year` YEAR NULL ,
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NULL DEFAULT 'Unpaid' ;

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES  (271
        ,1821
        ,3
        ,35000
        ,NULL
       ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (272
       ,1822
       ,4
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (273
       ,1823
       ,5
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (274
       ,1824
       ,6
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (275
       ,1825
       ,7
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
                        
VALUES (276
       ,1826
       ,8
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (271
       ,1827
       ,3
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (272
       ,1828
       ,4
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (273
       ,1829
       ,5
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (274
       ,1830
       ,6
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (275
       ,1831
       ,7
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (276
       ,1832
       ,8
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (271
       ,1833
       ,3
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (272
       ,1834
       ,4
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`)  
VALUES (273
       ,1835
       ,5
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (274
       ,1836
       ,6
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (275
       ,1837
       ,7
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`)  
VALUES (276
       ,1838
       ,8
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (271
       ,1839
       ,3
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (272
       ,1840
       ,4
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (273
       ,1841
       ,5
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (274
       ,1842
       ,6
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`)  
VALUES (275
       ,1843
       ,7
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (276
       ,1844
       ,8
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (271
       ,1845
       ,3
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (272
       ,1846
       ,4
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (273
       ,1847
       ,5
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (274
       ,1848
       ,6
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (275
       ,1849
       ,7
       ,35000
       ,NULL
      ,'Unpiad');

INSERT INTO `semester_fee` (`cdept_id`
                           ,`stud_id`
                           ,`semester`
                           ,`amount`
                           ,`paid_year`
                           ,`paid_status`) 
VALUES (276
       ,1850
       ,8
       ,35000
       ,NULL
      ,'Unpiad');
