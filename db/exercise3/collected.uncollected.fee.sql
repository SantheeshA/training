9. Show consolidated result for the following scenarios;
a) collected and uncollected semester fees amount per semester
for each college under an university. Result should be filtered
based on given year.

ALTER TABLE `university management`.`semester_fee` 
ADD COLUMN `balance_amount` DOUBLE NOT NULL AFTER `paid_status`;

ALTER TABLE employee ADD INDEX(PAid_YAER);

ALTER TABLE university ADD INDEX(university_name);

SELECT univ.university_name
      ,coll.name
      ,sem_fee.semester
      ,SUM(amount) AS 'Collected_fees' 
      ,SUM(balance_amount) AS 'Uncollected_fees' 
  FROM university univ
 INNER JOIN college coll
    ON univ.univ_code = coll.univ_code
 INNER JOIN student stud
    ON coll.id = stud.college_id 
 INNER JOIN semester_fee sem_fee
    ON stud.id = sem_fee.stud_id
 WHERE paid_year = 2020
   AND university_name = 'Anna University'
 GROUP BY coll.name

b) Collected semester fees amount for each university for the given 
    year  

ALTER TABLE employee ADD INDEX(PAid_YAER);

SELECT univ.univ_code
      ,univ.university_name
      ,SUM(sem_fee.amount) As Collected_Fees
      ,sem_fee.paid_year
  FROM university univ
  INNER JOIN college coll
    ON univ.univ_code = coll.univ_code
  INNER JOIN student stud
    ON coll.id = stud.college_id 
  INNER JOIN semester_fee sem_fee
    ON stud.id = sem_fee.stud_id
 WHERE paid_status = 'Paid'
   AND paid_year = '2020'
 GROUP BY univ.univ_code
         ,univ.university_name