7. Update paid_status and paid_year in SEMESTER_FEE table who
has done the payment. Entries should be updated based on student
roll_number.
a) Single Update - Update one student entry

UPDATE semester_fee sem_fee 
   SET paid_year = (2020) 
      ,paid_status = "Paid" 
WHERE (SELECT roll_number
         FROM student stud 
        WHERE sem_fee.stud_id = stud.id)
         IN ("12CS11")



b) Bulk Update - Update multiple students entries

UPDATE semester_fee sem_fee
   SET paid_year = (2019) 
      ,paid_status = "Paid" 
WHERE (SELECT roll_number
         FROM student stud
        WHERE sem_fee.stud_id = stud.id) 
         IN ("12CS12" 
            ,"12CS13"
            ,"12IT02"
            ,"12EC02"
            ,"12BM02");

c) paid_status value as ‘Paid’

SET paid_status = "Paid"  # paid_status value as ‘Paid’

d) paid_year value as ‘year format

SET paid_year = (2020) # paid_year value as ‘year format’
