5. List Students details along with their grade,CREDIT and gpa details
from all universities. Result should be sorted by college_name and
semester. Apply paging also.


SELECT stud.id
      ,stud.roll_number
      ,stud.name
      ,stud.gender
      ,stud.phone AS 'Student Contact'
      ,coll.name AS 'College Name'
      ,sem_res.semester
      ,sem_res.credits
      ,sem_res.grade
      ,sem_res.gpa
 FROM student stud
INNER JOIN semester_result sem_res
 ON stud.id = sem_res.stud_id
INNER JOIN college coll
 ON stud.COLLEGE_id = coll.id 
INNER JOIN university univ 
 ON coll.univ_code = univ.univ_code
 ORDER BY coll.name
         ,sem_res.semester
LIMIT 10
OFFSET 0