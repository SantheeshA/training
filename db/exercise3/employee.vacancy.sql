8. Find employee vacancy position in all the departments from all the
colleges and across the universities. Result should be populated with
following information.. Designation, rank, college, department and
university details

ALTER TABLE employee ADD INDEX(name);

SELECT univ.university_name
      ,dept.dept_name 
      ,coll.name AS college_name
      ,desig.rank
      ,desig.name AS desig_name
  FROM university univ
 INNER JOIN college coll
    ON univ.univ_code = coll.univ_code 
 INNER JOIN college_department coll_dept
    ON coll.id = coll_dept.college_id
 INNER JOIN employee emp
    ON coll_dept.cdept_id = emp.cdept_id 
 INNER JOIN designation desig
    ON emp.desig_id = desig.id 
 INNER JOIN department dept
    ON coll_dept.udept_code = dept.dept_code
 WHERE emp.name is NULL

