4. List Employees details from a particular university along with their
college and department details. Details should be sorted by rank and
college name

ALTER TABLE university ADD INDEX(university_name);

SELECT emp.id
      ,emp.name AS emp_name
      ,coll.name AS colege_name
      ,emp.dob
      ,desig.name AS desig_name
  FROM employee emp
 INNER JOIN college coll
    ON coll.ID = emp.college_id
 INNER JOIN university univ
    ON coll.univ_code = univ.univ_code
 INNER JOIN designation desig
    ON desig.id = emp.desig_id
 WHERE univ.university_name = 'Anna University' 
 ORDER BY coll.name 
         ,desig.rank
