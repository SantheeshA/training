2. Select final year students(Assume that universities has Engineering
Depts only) details who are studying under a particular university and
selected cities alone.
roll_number, name, gender, dob, email, phone, address,
COLLEGE_name, DEPARTMENT_name

ALTER TABLE university ADD INDEX(university_name);

SELECT stud.roll_number
	  ,stud.name
      ,stud.gender
      ,stud.dob
      ,stud.email
      ,stud.phone
      ,stud.address
      ,stud.academic_year AS 'Batch'
      ,col.name
      ,dept.dept_name
  FROM student stud
 INNER JOIN college col
    ON stud.college_id = col.id
 INNER JOIN university univ
    ON col.univ_code = univ.univ_code 
 INNER JOIN college_department col_dept
    ON stud.cdept_id = col_dept.cdept_id
 INNER JOIN department dept
    ON col_dept.udept_code = dept.dept_code
 WHERE stud.academic_year = '2022' 
   AND col.city = "Coimbatore"
   AND univ.university_name = "Anna University"