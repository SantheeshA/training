
10. Display below result in one SQL run

a) Shows students details who scored above 8 GPA for the given
semester
b) Shows students details who scored above 5 GPA for the given
semester

ALTER TABLE semester_result ADD INDEX(semester);


(SELECT stud.id
       ,stud.roll_number
       ,stud.name AS student_name
       ,stud.gender
       ,coll.code
       ,coll.name AS college_name
       ,sem_res.grade
       ,sem_res.gpa 
       ,sem_res.semester
   FROM student stud
  INNER JOIN college coll
     ON stud.college_id = coll.id
  INNER JOIN university univ
     ON coll.univ_code = univ.univ_code 
  INNER JOIN semester_result sem_res
     ON stud.id = sem_res.stud_id
  WHERE sem_res.semester = 3
 HAVING sem_res.gpa > 5)

UNION 

(SELECT stud.id
       ,stud.roll_number
       ,stud.name AS student_name
       ,stud.gender
       ,coll.code
       ,coll.name AS college_name
       ,sem_res.grade
       ,sem_res.gpa
       ,sem_res.semester
   FROM student stud
  INNER JOIN college coll
     ON stud.college_id = coll.id
  INNER JOIN university univ
     ON coll.univ_code = univ.univ_code 
  INNER JOIN semester_result sem_res
     ON stud.id = sem_res.stud_id
  WHERE sem_res.semester = 6
 HAVING sem_res.gpa > 8)