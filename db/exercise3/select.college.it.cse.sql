1. Select College details which are having IT / CSC departments across all
the universities. Result should have below details;(Assume that one of
the Designation name is ‘HOD’ in Designation table)
code, COLLEGE_name, university_name, city, state,
year_opened, DEPTARTMENT_name, hod_name

ALTER TABLE designation ADD INDEX(name);

SELECT col.code
      ,col.name As college_code
      ,univ.university_name
      ,col.city
      ,col.state
      ,col.year_opened
      ,dept.dept_name
      ,emp.name As hod_name
  FROM college col
 INNER JOIN university univ 
    ON col.univ_code = univ.univ_code
 INNER JOIN employee emp
    ON col.ID = emp.college_id
 INNER JOIN designation desig
    ON desig.ID = emp.desig_id
 INNER JOIN college_department col_dept
    ON col_dept.cdept_id = emp.cdept_id
 INNER JOIN department dept 
    ON col_dept.udept_code = dept.dept_code
 WHERE desig.name in ('HoD')
HAVING dept.dept_name = 'CSE' 
    OR dept.dept_name = 'IT'