1.Create Database\table and Rename\Drop Table

CREATE SCHEMA `College System Management`

CREATE TABLE `college system management`.`staff_table` (`staff_id` INT NOT NULL
                                                       ,`staff_name` VARCHAR(45) NOT NULL
                                                       ,`staff_subject` VARCHAR(45) NOT NULL
                                                       ,`staff_age` INT NOT NULL
                                                        ,PRIMARY KEY (`staff_id`));

CREATE TABLE `college system management`.`student_table` (`roll_no` INT NOT NULL
                                                         ,`stud_name` VARCHAR(45) NOT NULL
                                                         ,`dob` VARCHAR(45) NOT NULL
                                                         ,`year_of_joining` VARCHAR(45) NOT NULL
                                                         ,`staff_id` INT NOT NULL
                                                          ,PRIMARY KEY (`roll_no`)
                                                          ,FOREIGN KEY (`staff_id`)
                                                           REFERENCES staff_table(`staff_id`));

CREATE TABLE `college system management`.`department` (`dept_id` INT NOT NULL
                                                      ,`dept_name` VARCHAR(45) NOT NULL
                                                       ,PRIMARY KEY (`dept_id`)
                                                       ,FOREIGN KEY (`roll_no`)
                                                        REFERENCES staff_table(`roll_no`));


ALTER TABLE `college system management`.`stud_table` 
 RENAME TO  `college system management`.`student_table` ;

DROP TABLE `college system management`.`staff_table`
