12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)

SELECT * 
  FROM student_table
 CROSS JOIN  staff_table

SELECT *
  FROM student_table
 INNER JOIN  staff_table
    ON student_table.staff_id = staff_table.staff_id;

SELECT *
  FROM student_table
  LEFT JOIN  staff_table
    ON student_table.staff_id = staff_table.staff_id;

SELECT *
  FROM student_table
 RIGHT JOIN  staff_table
    ON student_table.staff_id = staff_table.staff_id;
