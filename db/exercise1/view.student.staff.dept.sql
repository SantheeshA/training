14.Create VIEW for your Join Query and select values from VIEW

CREATE VIEW `[All_Details]` AS
SELECT dept_name 
      ,stud_name 
      ,dob
      ,staff_name
  FROM department
  JOIN student_table 
    ON student_table.roll_no = department.roll_no
  JOIN staff_table 
    ON staff_table.staff_id = student_table.staff_id

CREATE VIEW `[New_Students]` AS
      SELECT roll_no, stud_name, dob
        FROM student_table
       WHERE year_of_joining = "2019"

SELECT * FROM `[New_Students]`
    DROP VIEW `[New_Students]`