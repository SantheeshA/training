2.Alter Table with Add new Column and Modify\Rename\Drop column

ALTER TABLE `college system management`.`staff_table` 
ADD COLUMN `staff_subject` VARCHAR(45) NOT NULL AFTER `staff_name`;

ALTER TABLE `college system management`.`student_table` 
CHANGE COLUMN `student_name` `stud_name` VARCHAR(45) NOT NULL ;

ALTER TABLE `college system management`.`student_table` 
CHANGE COLUMN `dob` `dob` DATE NOT NULL ,
CHANGE COLUMN `year_of_joining` `year_of_joining` DATE NOT NULL ;

ALTER TABLE `college system management`.`staff_table` 
DROP COLUMN `year_of_joining`;
