9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )

SELECT *
  FROM staff_table
 WHERE staff_name = "Makeshwaran" 
   AND staff_age="25"

SELECT * 
  FROM staff_table
 WHERE staff_age="35"  
    OR staff_age="25"

SELECT * 
  FROM student_table
 WHERE NOT year_of_joining = "2018"

SELECT *
  FROM student_table
 WHERE stud_name 
 LIKE "a%"

SELECT * 
  FROM student_table
 WHERE stud_name IN 
     ('Abisek'
     ,'Dharun')

SELECT stud_name 
  FROM student_table
 WHERE staff_id = ANY 
      (SELECT staff_id 
         FROM staff_table 
        WHERE staff_age = "25" );

SELECT *
  FROM student_table 
 WHERE stud_name 
 LIKE "Abi___"