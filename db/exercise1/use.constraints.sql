3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, KEY, INDEX

ALTER TABLE `college system management`.`staff_table` 
CHANGE COLUMN `staff_id` `staff_id` INT NOT NULL ,
   ADD PRIMARY KEY (`staff_id`),

ALTER COLUMN staff_subject 
SET DEFAULT 'Software Engineer',

ADD CHECK (staff_age>=25);

ALTER TABLE `college system management`.`student_table`
ADD FOREIGN KEY (staff_id) 
      REFERENCES staff_table(staff_id),

ADD UNIQUE (roll_no);

CREATE INDEX idx_stud_name
       ON student_table(stud_name);
