5.Insert Records into Table

INSERT INTO `college system management`.`student_table` (`roll_no`
                                                        ,`stud_name`
                                                        ,`dob`
                                                        ,`year_of_joining`
                                                        ,`staff_id`) 
VALUES ('12091'
       ,'Aravind'
       ,'18/09/2000'
       ,'2018'
       ,'201');
INSERT INTO `college system management`.`student_table` (`roll_no` 
                                                        ,`stud_name`
                                                        ,`dob`
                                                        ,`year_of_joining`
                                                        ,`staff_id`) 
VALUES ('12092'
       ,'Bupathi'
       ,'01/12/1999'
       ,'2018'
       ,'204');
INSERT INTO `college system management`.`student_table` (`roll_no`
                                                        ,`stud_name`
                                                        ,`dob`
                                                        ,`year_of_joining`
                                                        ,`staff_id`) 
VALUES ('12092'
       ,'Abisek'
       ,'20/03/1999'
       ,'2018'
       ,'202');
INSERT INTO `college system management`.`student_table` (`roll_no`
                                                        ,`stud_name`
                                                        ,`dob`
                                                        ,`year_of_joining`
                                                        ,`staff_id`) 
VALUES ('12092'
       ,'Dharun'
       ,'01/09/2000'
       ,'2018'
       ,'203');
INSERT INTO `college system management`.`student_table` (`roll_no`
                                                        ,`stud_name`
                                                        ,`dob`
                                                        ,`year_of_joining`
                                                        ,`staff_id`) 
VALUES ('12092'
       ,'Feroz'
       ,'02/01/1999'
       ,'2019'
       ,'206');


INSERT INTO `college system management`.`department` (`dept_id`
                                                     ,`dept_name`
                                                     ,`roll_no`) 
VALUES (1091
      ,'Chemistry'
       ,12091);
INSERT INTO `college system management`.`department` (`dept_id`
                                                     ,`dept_name`
                                                     ,`roll_no`) 
VALUES (1092
      ,'Maths'
       ,12093);
INSERT INTO `college system management`.`department` (`dept_id`
                                                     ,`dept_name`
                                                     ,`roll_no`)
VALUES (1093
      ,'BioMedical'
       ,12092);
INSERT INTO `college system management`.`department` (`dept_id`
                                                     ,`dept_name`
                                                     ,`roll_no`) 
VALUES (1094
      ,'Computer Science'
       ,12094);
INSERT INTO `college system management`.`department` (`dept_id`
                                                     ,`dept_name`
                                                     ,`roll_no`) 
VALUES (1095
      ,'English'
       ,12095);
INSERT INTO `college system management`.`staff_table` (`staff_id`
                                                      ,`staff_name`
                                                      ,`staff_subject`
                                                      ,`staff_age`
                                                      ,`salary`) 
VALUES (201
      ,'Makeshwaran'
      ,'Organ Chemistry'
      ,'27'
       ,25000);
INSERT INTO `college system management`.`staff_table` (`staff_id`
                                                      ,`staff_name`
                                                      ,`staff_subject`
                                                      ,`staff_age`
                                                      ,`salary`) 
VALUES (202
      ,'Suresh'
      ,'BioMedical'
      ,'31'
       ,40000);
INSERT INTO `college system management`.`staff_table` (`staff_id`
                                                      ,`staff_name`
                                                      ,`staff_subject`
                                                      ,`staff_age`
                                                      ,`salary`) 
VALUES (203
      ,'Karunan'
      ,'English'
      ,'26'
       ,35000);
INSERT INTO `college system management`.`staff_table` (`staff_id`
                                                      ,`staff_name`
                                                      ,`staff_subject`
                                                      ,`staff_age`
                                                      ,`salary`) 
VALUES (204
      ,'Karthik'
      ,'Software Engineer'
      ,'36'
       ,45000);
INSERT INTO `college system management`.`staff_table` (`staff_id`
                                                      ,`staff_name`
                                                      ,`staff_subject`
                                                      ,`staff_age`
                                                      ,`salary`) 
VALUES (205
      ,'Malavan'
      ,'Discrete Maths'
      ,'28'
       ,50000);
INSERT INTO `college system management`.`staff_table` (`staff_id`
                                                      ,`staff_name`
                                                      ,`staff_subject`
                                                      ,`staff_age`
                                                      ,`salary`)  
VALUES (206
      ,'Athavan'
      ,'Engineering Maths'
      ,'29'
       ,50000);

