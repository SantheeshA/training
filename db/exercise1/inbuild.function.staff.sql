10.USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM

SELECT MIN(staff_age)
  FROM staff_table

SELECT MAX(staff_age)
  FROM staff_table

SELECT AVG(staff_age)
  FROM staff_table

SELECT COUNT(staff_id)
  FROM staff_table

SELECT *
  FROM student_table
 ORDER BY roll_no 
   ASC LIMIT 1

SELECT *
  FROM student_table
 ORDER BY roll_no 
  DESC LIMIT 1

SELECT SUM(salary)
  FROM staff_table