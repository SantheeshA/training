13.At-least use 3 tables for Join
Union 2 Different table with same Column name (use Distinct,ALL)

SELECT department.dept_name
      ,student_table.stud_name
      ,staff_table.staff_name
  FROM department
  JOIN student_table 
    ON student_table.roll_no = department.roll_no
  JOIN staff_table 
    ON staff_table.staff_id = student_table.staff_id

CREATE TABLE `college system management`.`student_table2` (`roll_no` INT NOT NULL
                                                          ,`stud_name` VARCHAR(45) NOT NULL
                                                          ,`dob` VARCHAR(45) NOT NULL
                                                          ,`yaer_of_joining` VARCHAR(45) NOT NULL
                                                           ,PRIMARY KEY (`roll_no`));
INSERT INTO `college system management`.`student_table2` (`roll_no`
                                                         ,`stud_name`
                                                         ,`dob`
                                                         ,`year_of_joining`) 
VALUES ('12110'
       ,'Kailash'
       ,'12/01/2000'
       ,'2018');
INSERT INTO `college system management`.`student_table2` (`roll_no`
                                                         ,`stud_name`
                                                         ,`dob`
                                                         ,`year_of_joining`) 
VALUES ('12111'
       ,'Marutha'
       ,'23/09/1997'
       ,'2017');
INSERT INTO `college system management`.`student_table2` (`roll_no`
                                                         ,`stud_name`
                                                         ,`dob`
                                                         ,`year_of_joining`)
VALUES ('12112',
       ,'Suresh'
       ,'01/12/1999'
       ,'2015');
INSERT INTO `college system management`.`student_table2` (`roll_no`
                                                         ,`stud_name`
                                                         ,`dob`
                                                         ,`year_of_joining`)
VALUES ('12113'
       ,'Kumarvraman'
       ,'09/011999'
       ,'2019');
INSERT INTO `college system management`.`student_table2` (`roll_no`
                                                         ,`stud_name`
                                                         ,`dob`
                                                         ,`year_of_joining`)
VALUES ('12114'
       ,'Kala'
       ,'03/03/1997'
       ,'2016');

(SELECT 'student_table' 
       ,roll_no
       ,stud_name
       ,dob
       ,year_of_joining
       ,staff_id
  FROM student_table)
  UNION
(SELECT 'student_table2'
       ,roll_no
       ,stud_name
       ,dob
       ,year_of_joining
       ,staff_id
   FROM student_table2)

(SELECT stud_name 
   FROM student_table)
UNION ALL
(SELECT stud_name 
  FROM student_table2
 ORDER BY stud_name)